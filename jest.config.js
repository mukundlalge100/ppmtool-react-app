module.exports = {
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/__mocks__/fileMock.js",
    "\\.(css|less|scss)$": "identity-obj-proxy",
    "\\.svg$": "<rootDir>/__mocks__/svgrMock.js",
  },
  transform: {
    "\\.(js|jsx)$": "babel-jest",
  },
  setupFilesAfterEnv: ["./EnzymeSetUp.js"],
  collectCoverage: true,
  coverageReporters: ["html"],
};
