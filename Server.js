const express = require("express");
const path = require("path");

const app = express();
const port = process.env.PORT || 3000; // Heroku will need the PORT environment variable

app.listen(port, () => console.log(`App is live on port ${port}!`));

app.use(express.static(path.join(__dirname, "dist")));
app.get("*", (request, response) => {
  response.sendFile(path.join(__dirname, "dist", "index.html"));
});
