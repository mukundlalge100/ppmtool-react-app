import { Provider } from "react-redux";
import React from "react";
import configureStore from "redux-mock-store";
import createSagaMiddleware from "redux-saga";
import { mount } from "enzyme";
import rootSaga from "../../src/sagas/rootSaga";
import { BrowserRouter } from "react-router-dom";
import thunk from "redux-thunk";

const sagaMiddleware = createSagaMiddleware();
const mockStore = configureStore([sagaMiddleware, thunk]);

/**
 * Utility method for getting wrapper for component
 * @param props
 * @param Component
 * @returns MountWrapper
 */
const setup = (props, Component, recieveStore) => {
  const store = mockStore({ ...recieveStore });
  sagaMiddleware.run(rootSaga);

  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <Component {...props} />
      </BrowserRouter>
    </Provider>
  );
  return {
    wrapper,
    store,
  };
};

export { setup };
