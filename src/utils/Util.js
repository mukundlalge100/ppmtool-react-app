import axios from "../api/PPMTool";

export const updateObject = (oldObject, newObject) => {
  return {
    ...oldObject,
    ...newObject,
  };
};

export const setAuthToken = (token) => {
  if (token) {
    token = `Bearer ${token}`;
    axios.defaults.headers.common["Authorization"] = token;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
};

export const isEmpty = (value) => {
  return (
    value === undefined ||
    value === "undefined" ||
    value === null ||
    (typeof value === "object" && Object.keys(value).length === 0) ||
    (typeof value === "string" && value.trim().length === 0)
  );
};

export const formatDate = (dateString) => {
  const date = new Date(dateString);
  const newDate = new Date(date.getTime());
  let month = newDate.getUTCMonth();
  let day = newDate.getUTCDate();
  let year = newDate.getUTCFullYear();
  const dateFormat =
    year + "-" + ("0" + (month + 1)).slice(-2) + "-" + ("0" + day).slice(-2);
  return dateFormat;
};

export const randomIntFromRange = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

export const randomColor = (colors) => {
  return colors[Math.floor(Math.random() * colors.length)];
};

export const distance = (x1, y1, x2, y2) => {
  const xDist = x2 - x1;
  const yDist = y2 - y1;
  return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
};
