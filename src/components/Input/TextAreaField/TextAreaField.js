import React from "react";
import PropTypes from "prop-types";
import classes from "../Input.module.scss";

// TEXTAREA INPUT FUNCTIONAL COMPONENT...
const TextAreaField = (props) => {
  return (
    <div className={`${classes.Input}`}>
      <textarea
        className={`${classes.Input__InputElement}  ${
          props.error || (props.meta.error && props.meta.touched)
            ? classes.Input__Invalid
            : ""
        }`}
        placeholder={props.placeholder}
        {...props.input}
        name={props.input.name}
      />
      <label className={classes.Input__Label}>{props.label}</label>

      {props.info ? (
        <small className={classes.Input__Text}>{props.info}</small>
      ) : null}

      {props.error || (props.meta.error && props.meta.touched) ? (
        <p className={classes.Input__IsInvalid}>
          {props.error || props.meta.error}
        </p>
      ) : null}
    </div>
  );
};

TextAreaField.propTypes = {
  // REQUIRED PROPS...
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,

  // OPTIONAL PROPS...
  error: PropTypes.string,
  info: PropTypes.string,
};

export default TextAreaField;
