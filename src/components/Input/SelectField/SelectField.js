import React from "react";
import PropTypes from "prop-types";
import classes from "../Input.module.scss";
// SELECT INPUT FUNCTIONAL COMPONENT...
const SelectField = (props) => {
  // SELECT OPTIONS FOR SELECT ELEMENT ...
  const selectOptions = props.options.map((option) => {
    return (
      <option value={option.value} key={option.label}>
        {option.label}
      </option>
    );
  });
  return (
    <div className={`${classes.Input}`}>
      <select
        className={`${classes.Input__InputElement}  ${
          props.error ? classes.Input__Invalid : ""
        }`}
        name={props.input.name}
        id={props.id}
        {...props.input}
      >
        {selectOptions}
      </select>

      <label className={classes.Input__Label}>{props.label}</label>

      {props.info ? (
        <small className={classes.Input__Text}>{props.info}</small>
      ) : null}

      {props.error ? (
        <p className={classes.Input__IsInvalid}>{props.error}</p>
      ) : null}
    </div>
  );
};

SelectField.propTypes = {
  // REQUIRED PROPS ...
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  input: PropTypes.object.isRequired,

  // OPTIONAL...
  inputProps: PropTypes.object,
  error: PropTypes.string,
  info: PropTypes.string,
  disabled: PropTypes.bool,
};

SelectField.defaultProps = {
  disabled: false,
};
export default SelectField;
