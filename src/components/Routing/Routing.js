import React, { lazy } from "react";
import { Route, Redirect, Switch, withRouter } from "react-router-dom";
import Landing from "../../components/Layouts/Landing/Landing";
import ProjectBoard from "../../containers/ProjectBoard/ProjectBoard";
import UpdateProjectTask from "../../containers/ProjectTask/UpdateProjectTask/UpdateProjectTask";
import PageNotFound from "../../components/PageNotFound/PageNotFound";
import Dashboard from "../../containers/Dashboard/Dashboard";
import AddProjectTask from "../../containers/ProjectTask/AddProjectTask/AddProjectTask";
import SignUp from "../../containers/Auth/SignUp/SignUp";
import LogIn from "../../containers/Auth/LogIn/LogIn";
import AddProject from "../../containers/Project/AddProject/AddProject";
import EditProject from "../../containers/Project/EditProject/EditProject";

const Routing = (props) => {
  let routes;
  if (props.isAuthenticated) {
    routes = (
      // PRIVATE ROUTES ...
      <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/addProject" component={AddProject} />
        <Route
          exact
          path="/projectBoard/:projectIdentifier"
          component={ProjectBoard}
        />
        <Route
          exact
          path="/projectBoard/:projectIdentifier/:projectSequence"
          component={UpdateProjectTask}
        />
        <Route
          exact
          path="/addProjectTask/:projectIdentifier"
          component={AddProjectTask}
        />
        <Route exact path="/updateProject/:projectId" component={EditProject} />
        <Route exact path="/pageNotFound" component={PageNotFound} />
        <Redirect exact to="/pageNotFound" />
      </Switch>
    );
  } else {
    routes = (
      // PUBLIC ROUTES ...
      <Switch>
        <Route exact path="/" component={Landing} />
        <Route exact path="/signup" component={SignUp} />
        <Route exact path="/login" component={LogIn} />
        <Redirect exact to="/" />
      </Switch>
    );
  }
  return routes;
};

export default Routing;
