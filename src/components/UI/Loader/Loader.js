import React from "react";
import classes from "./Loader.module.scss";
import { ReactComponent as Spinner } from "../../../assets/SVG/Spinner.svg";

const Loader = () => <Spinner className={classes.Spinner_Svg} />;

export default Loader;
