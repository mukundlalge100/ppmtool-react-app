import React from "react";
import classes from "./SideDrawer.module.scss";
import BackDrop from "../../UI/Bakdrop/Backdrop";

const SideDrawer = props => {
  let showHideSideDrawer = [classes.SideDrawer, classes.Close].join(" ");

  if (props.showHideSideDrawer) {
    showHideSideDrawer = [classes.SideDrawer, classes.Open].join(" ");
  }
  return (
    <React.Fragment>
      <BackDrop
        show={props.showHideSideDrawer}
        closeBackDrop={props.sideDrawerCloseHandler}
      />
      <div className={showHideSideDrawer}>{props.children}</div>
    </React.Fragment>
  );
};
export default SideDrawer;
