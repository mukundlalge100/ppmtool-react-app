import React from "react";
import { Link } from "react-router-dom";
import utilClasses from "../../../utils/Util.module.scss";
import classes from "./Landing.module.scss";
import Canvas from "../../../containers/Canvas/Canvas";

const Landing = (props) => {
  return (
    <div className={classes.Landing}>
      <Canvas className={classes.Landing_Canvas} />
      <div className={classes.Landing_Content}>
        <header className={classes.Landing_Header}>
          <h1 className={utilClasses.Primary__Heading}>
            Personal Project Management Tool
          </h1>
          <hr />
          <p
            className={utilClasses.Paragraph}
            style={{ justifySelf: "center", fontSize: "2rem" }}
          >
            Personal Project Management Tool gives convenient way create your
            own project , it's project task.
            <br /> Using ppmtool you can update,delete or create project and
            it's task .
          </p>
        </header>
        <div className={classes.Landing_LinkContainer}>
          <Link to="/signup" className={utilClasses.Button}>
            SignUp
          </Link>
          <Link to="/login" className={utilClasses.Button}>
            Login
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Landing;
