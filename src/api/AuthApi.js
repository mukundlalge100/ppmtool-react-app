import axios from "./PPMTool";

export const logInRequest = (userData) => {
  return axios.post("/api/user/login", userData);
};
export const signUpRequest = (userData) => {
  return axios.post("/api/user/signup", userData);
};
export const logOutRequest = () => {
  return axios.get("/api/user/logout");
};
