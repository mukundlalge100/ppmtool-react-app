import axios from "axios";

const PPMTool = axios.create({
  // baseURL: "http://localhost:8080/ppmtool",
  baseURL: "https://ppmtool-spring-boot.herokuapp.com/ppmtool",
  timeout: 5 * 60 * 1000,
});

export default PPMTool;
