import axios from "./PPMTool";

export const getProjectTasksRequest = (projectIdentifier) => {
  return axios.get(`/api/backlog/${projectIdentifier}`);
};

export const createProjectTaskRequest = (projectTask, projectIdentifier) => {
  return axios.post(`/api/backlog/${projectIdentifier}`, projectTask);
};

export const updateProjectTaskRequest = (
  projectTask,
  projectIdentifier,
  projectSequence
) => {
  return axios.patch(
    `/api/backlog/${projectIdentifier}/${projectSequence}`,
    projectTask
  );
};

export const deleteProjectTaskRequest = (
  projectIdentifier,
  projectSequence
) => {
  return axios.delete(`/api/backlog/${projectIdentifier}/${projectSequence}`);
};

export const getProjectTaskRequest = (projectIdentifier, projectSequence) => {
  return axios.get(`/api/backlog/${projectIdentifier}/${projectSequence}`);
};
