import axios from "./PPMTool";

export const getProjectsRequest = (pageNumber) => {
  return axios.get(`/api/project/all/${pageNumber}`);
};

export const getProjectRequest = (projectIdentifier) => {
  return axios.get(`/api/project/${projectIdentifier}`);
};

export const updateProjectRequest = (project, projectIdentifier) => {
  return axios.put(`/api/project/${projectIdentifier}`, project);
};

export const deleteProjectRequest = (projectIdentifier) => {
  return axios.delete(`/api/project/${projectIdentifier}`);
};

export const createProjectRequest = (project) => {
  return axios.post(`/api/project`, project);
};
