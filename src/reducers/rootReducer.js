import { combineReducers } from "redux";
import authReducer from "./AuthReducer";
import signUpReducer from "./SignUpReducer";
import projectTaskReducer from "./ProjectTaskReducer";
import projectReducer from "./ProjectReducer";

export default combineReducers({
  authReducer,
  signUpReducer,
  projectReducer,
  projectTaskReducer,
});
