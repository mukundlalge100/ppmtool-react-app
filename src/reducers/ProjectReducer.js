import {
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAIL,
  CLEAR_CREATE_PROJECT_ERRORS,
  UPDATE_PROJECT_REQUEST,
  UPDATE_PROJECT_SUCCESS,
  UPDATE_PROJECT_FAIL,
  GET_PROJECT_REQUEST,
  GET_PROJECT_SUCCESS,
  GET_PROJECT_FAIL,
  GET_PROJECTS_REQUEST,
  GET_PROJECTS_SUCCESS,
  GET_PROJECTS_FAIL,
  DELETE_PROJECT_REQUEST,
  DELETE_PROJECT_SUCCESS,
  DELETE_PROJECT_FAIL,
} from "../constants/ActionTypes";

import { updateObject } from "../utils/Util";

const initialState = {
  projects: [],
  totalPages: null,
  itemsCountPerPage: null,
  totalItemsCount: null,
  project: {},
  isLoading: false,
  errors: {},
};

const ProjectReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PROJECT_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case CREATE_PROJECT_SUCCESS:
      return updateObject(state, {
        project: action.project,
        isLoading: false,
      });
    case CREATE_PROJECT_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });
    case CLEAR_CREATE_PROJECT_ERRORS:
      return updateObject(state, {
        errors: {},
      });
    case UPDATE_PROJECT_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case UPDATE_PROJECT_SUCCESS:
      return updateObject(state, {
        project: action.project,
        isLoading: false,
      });
    case UPDATE_PROJECT_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });
    case DELETE_PROJECT_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case DELETE_PROJECT_SUCCESS:
      return updateObject(state, {
        isLoading: false,
      });
    case DELETE_PROJECT_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });
    case GET_PROJECT_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case GET_PROJECT_SUCCESS:
      return updateObject(state, {
        project: action.project,
        isLoading: false,
      });
    case GET_PROJECT_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });
    case GET_PROJECTS_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case GET_PROJECTS_SUCCESS:
      return updateObject(state, {
        projects: action.projects,
        itemsCountPerPage: action.itemsCountPerPage,
        totalPages: action.totalPages,
        totalItemsCount: action.totalItemsCount,
        isLoading: false,
      });
    case GET_PROJECTS_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });
    default:
      return state;
  }
};

export default ProjectReducer;
