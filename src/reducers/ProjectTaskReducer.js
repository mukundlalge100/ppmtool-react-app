import {
  GET_PROJECT_TASKS_REQUEST,
  GET_PROJECT_TASKS_SUCCESS,
  GET_PROJECT_TASKS_FAIL,
  GET_PROJECT_TASK_REQUEST,
  GET_PROJECT_TASK_SUCCESS,
  GET_PROJECT_TASK_FAIL,
  CREATE_PROJECT_TASK_REQUEST,
  CREATE_PROJECT_TASK_SUCCESS,
  CREATE_PROJECT_TASK_FAIL,
  CLEAR_CREATE_PROJECT_TASK_ERRORS,
  UPDATE_PROJECT_TASK_REQUEST,
  UPDATE_PROJECT_TASK_SUCCESS,
  UPDATE_PROJECT_TASK_FAIL,
  DELETE_PROJECT_TASK_REQUEST,
  DELETE_PROJECT_TASK_SUCCESS,
  DELETE_PROJECT_TASK_FAIL,
} from "../constants/ActionTypes";

import { updateObject } from "../utils/Util";

const initialState = {
  projectTasks: [],
  projectTask: {},
  isLoading: false,
  errors: {},
};

const ProjectTaskReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROJECT_TASKS_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case GET_PROJECT_TASKS_SUCCESS:
      return updateObject(state, {
        projectTasks: action.projectTasks,
        isLoading: false,
      });
    case GET_PROJECT_TASKS_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });

    case GET_PROJECT_TASK_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case GET_PROJECT_TASK_SUCCESS:
      return updateObject(state, {
        projectTask: action.projectTask,
        isLoading: false,
      });
    case GET_PROJECT_TASK_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });

    case CREATE_PROJECT_TASK_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case CREATE_PROJECT_TASK_SUCCESS:
      return updateObject(state, {
        projectTask: action.data,
        isLoading: false,
      });
    case CREATE_PROJECT_TASK_FAIL:
      return updateObject(state, {
        errors: action.errors,
        isLoading: false,
      });

    case UPDATE_PROJECT_TASK_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case UPDATE_PROJECT_TASK_SUCCESS:
      return updateObject(state, {
        projectTask: action.data,
        isLoading: false,
      });
    case UPDATE_PROJECT_TASK_FAIL:
      return updateObject(state, {
        errors: action.errors,
        isLoading: false,
      });

    case DELETE_PROJECT_TASK_REQUEST:
      return updateObject(state, {});
    case DELETE_PROJECT_TASK_SUCCESS:
      return updateObject(state, {
        projectTasks: state.projectTasks.filter(
          (projectTask) =>
            projectTask.projectSequence !== action.projectSequence
        ),
      });
    case DELETE_PROJECT_TASK_FAIL:
      return updateObject(state, {
        errors: action.errors,
      });

    case CLEAR_CREATE_PROJECT_TASK_ERRORS:
      return updateObject(state, {
        errors: {},
      });
    default:
      return state;
  }
};

export default ProjectTaskReducer;
