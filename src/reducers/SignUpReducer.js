import {
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  CLEAR_SIGNUP_ERRORS,
} from "../constants/ActionTypes";

import { updateObject } from "../utils/Util";

const initialState = {
  data: {},
  isLoading: false,
  errors: {},
};

const SignUpReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case SIGNUP_SUCCESS:
      return updateObject(state, {
        data: action.data,
        isLoading: false,
      });
    case SIGNUP_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });
    case CLEAR_SIGNUP_ERRORS:
      return updateObject(state, {
        errors: {},
      });
    default:
      return state;
  }
};

export default SignUpReducer;
