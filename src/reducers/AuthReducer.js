import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAIL,
  CLEAR_LOGIN_ERRORS,
  CHECK_LOGIN_STATE_REQUEST,
  CHECK_LOGIN_STATE_SUCCESS,
} from "../constants/ActionTypes";

import { updateObject, isEmpty } from "../utils/Util";

const initialState = {
  user: {},
  isLoading: false,
  errors: {},
  isLoadComponent: false,
  isAuthenticated: false,
};

const LogInReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case LOGIN_SUCCESS:
      return updateObject(state, {
        user: action.user,
        isLoading: false,
        isLoadComponent: true,
        isAuthenticated: !isEmpty(action.user),
      });
    case LOGIN_FAIL:
      return updateObject(state, {
        isLoading: false,
        isLoadComponent: true,
        errors: action.errors,
      });
    case CLEAR_LOGIN_ERRORS:
      return updateObject(state, {
        errors: {},
        isLoading: false,
      });
    case LOGOUT_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case LOGOUT_SUCCESS:
      return updateObject(state, {
        user: {},
        isLoading: false,
        isAuthenticated: false,
      });
    case LOGOUT_FAIL:
      return updateObject(state, {
        isLoading: false,
        errors: action.errors,
      });
    case CHECK_LOGIN_STATE_REQUEST:
      return updateObject(state, {
        isLoading: true,
      });
    case CHECK_LOGIN_STATE_SUCCESS:
      return updateObject(state, {
        isLoading: false,
        isLoadComponent: true,
      });
    default:
      return state;
  }
};

export default LogInReducer;
