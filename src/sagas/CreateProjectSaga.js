import { put, takeLatest, call } from "redux-saga/effects";
import { createProjectRequest } from "../api/ProjectApi";
import {
  CREATE_PROJECT_FAIL,
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
} from "../constants/ActionTypes";

export function* createProject(action) {
  try {
    const response = yield call(createProjectRequest, action.project);
    yield put({
      type: CREATE_PROJECT_SUCCESS,
      project: response.data,
    });
    action.history.push("/");
  } catch (error) {
    yield put({
      type: CREATE_PROJECT_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchCreateProject() {
  yield takeLatest(CREATE_PROJECT_REQUEST, createProject);
}
