import { put, takeLatest } from "redux-saga/effects";
import {
  CLEAR_LOGIN_ERRORS,
  CLEAR_LOGIN_ERRORS_REQUEST,
} from "../constants/ActionTypes";

function* clearLogInErrors() {
  yield put({ type: CLEAR_LOGIN_ERRORS });
}

export default function* watchClearLogInErrors() {
  yield takeLatest(CLEAR_LOGIN_ERRORS_REQUEST, clearLogInErrors);
}
