import { put, takeLatest } from "redux-saga/effects";
import {
  CLEAR_CREATE_PROJECT_TASK_ERRORS,
  CLEAR_CREATE_PROJECT_TASK_ERRORS_REQUEST,
} from "../constants/ActionTypes";

function* clearCreateProjectTaskErrors() {
  yield put({ type: CLEAR_CREATE_PROJECT_TASK_ERRORS });
}

export default function* watchClearCreateProjectTaskErrors() {
  yield takeLatest(
    CLEAR_CREATE_PROJECT_TASK_ERRORS_REQUEST,
    clearCreateProjectTaskErrors
  );
}
