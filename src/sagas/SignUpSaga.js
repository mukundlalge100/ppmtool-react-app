import { put, takeLatest, call } from "redux-saga/effects";
import { signUpRequest } from "../api/AuthApi";

import {
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
} from "../constants/ActionTypes";

export function* signUp(action) {
  try {
    const response = yield call(signUpRequest, action.userData);
    yield put({ type: SIGNUP_SUCCESS, data: response.data });
    action.history.push("/login");
  } catch (error) {
    yield put({ type: SIGNUP_FAIL, errors: error.response.data });
  }
}

export default function* watchSignUp() {
  yield takeLatest(SIGNUP_REQUEST, signUp);
}
