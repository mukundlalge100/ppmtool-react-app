import { put, takeLatest, call } from "redux-saga/effects";
import { getProjectTasksRequest } from "../api/BacklogApi";
import {
  GET_PROJECT_TASKS_REQUEST,
  GET_PROJECT_TASKS_SUCCESS,
  GET_PROJECT_TASKS_FAIL,
} from "../constants/ActionTypes";

export function* getProjectTasks(action) {
  try {
    const response = yield call(
      getProjectTasksRequest,
      action.projectIdentifier
    );
    yield put({ type: GET_PROJECT_TASKS_SUCCESS, projectTasks: response.data });
  } catch (error) {
    yield put({
      type: GET_PROJECT_TASKS_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchGetProjectTasks() {
  yield takeLatest(GET_PROJECT_TASKS_REQUEST, getProjectTasks);
}
