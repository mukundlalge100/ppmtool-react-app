import { put, takeLatest, call } from "redux-saga/effects";
import { setAuthToken } from "../utils/Util";
import jwtDecode from "jwt-decode";
import {
  CHECK_LOGIN_STATE_REQUEST,
  CHECK_LOGIN_STATE_SUCCESS,
  LOGIN_SUCCESS,
  LOGIN_REQUEST,
  LOGOUT_REQUEST,
  CHECK_EXPIRY_TIME_OF_TOKEN_REQUEST,
} from "../constants/ActionTypes";

function* checkLogInState(action) {
  if (localStorage.jwtToken) {
    setAuthToken(localStorage.jwtToken);
    const user = jwtDecode(localStorage.jwtToken);
    const currentTime = Date.now() / 1000;
    if (user.exp < currentTime) {
      setAuthToken(false);
      localStorage.removeItem("jwtToken");
      yield put({ type: LOGOUT_REQUEST, history: action.history });
      return;
    }
    yield put({ type: LOGIN_SUCCESS, user });
    yield put({ type: CHECK_LOGIN_STATE_SUCCESS });
  } else {
    yield put({ type: CHECK_LOGIN_STATE_SUCCESS });
  }
}

export default function* watchCheckLogInState() {
  yield takeLatest(CHECK_LOGIN_STATE_REQUEST, checkLogInState);
}
