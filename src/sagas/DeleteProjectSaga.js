import { put, takeLatest, call } from "redux-saga/effects";
import { deleteProjectRequest } from "../api/ProjectApi";
import { getProjects } from "../actions/ProjectActions";
import {
  DELETE_PROJECT_FAIL,
  DELETE_PROJECT_REQUEST,
  DELETE_PROJECT_SUCCESS,
} from "../constants/ActionTypes";

export function* deleteProject(action) {
  try {
    const response = yield call(deleteProjectRequest, action.projectIdentifier);
    if (response.data.success) {
      yield put({
        type: DELETE_PROJECT_SUCCESS,
      });
      yield put(getProjects(action.currentPage));
    }
  } catch (error) {
    yield put({
      type: DELETE_PROJECT_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchDeleteProject() {
  yield takeLatest(DELETE_PROJECT_REQUEST, deleteProject);
}
