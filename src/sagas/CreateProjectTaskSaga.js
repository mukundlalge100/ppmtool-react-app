import { put, takeLatest, call } from "redux-saga/effects";
import { createProjectTaskRequest } from "../api/BacklogApi";
import {
  CREATE_PROJECT_TASK_REQUEST,
  CREATE_PROJECT_TASK_SUCCESS,
  CREATE_PROJECT_TASK_FAIL,
} from "../constants/ActionTypes";

export function* createProjectTask(action) {
  try {
    const response = yield call(
      createProjectTaskRequest,
      action.projectTask,
      action.projectIdentifier
    );
    yield put({
      type: CREATE_PROJECT_TASK_SUCCESS,
      projectTask: response.data,
    });
    action.history.push("/");
  } catch (error) {
    yield put({
      type: CREATE_PROJECT_TASK_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchCreateProjectTask() {
  yield takeLatest(CREATE_PROJECT_TASK_REQUEST, createProjectTask);
}
