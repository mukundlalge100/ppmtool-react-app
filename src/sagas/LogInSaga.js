import { put, takeLatest, call } from "redux-saga/effects";
import { logInRequest } from "../api/AuthApi";
import { setAuthToken } from "../utils/Util";
import jwtDecode from "jwt-decode";
import {
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGIN_REQUEST,
} from "../constants/ActionTypes";

export function* logIn(action) {
  try {
    const response = yield call(logInRequest, action.userData);
    const { token } = response.data;
    localStorage.setItem("jwtToken", token);
    setAuthToken(token);
    const user = jwtDecode(token);
    yield put({ type: LOGIN_SUCCESS, user });
    action.history.push("/");
  } catch (error) {
    yield put({
      type: LOGIN_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchLogIn() {
  yield takeLatest(LOGIN_REQUEST, logIn);
}
