import { put, takeLatest, call } from "redux-saga/effects";
import { deleteProjectTaskRequest } from "../api/BacklogApi";
import {
  DELETE_PROJECT_TASK_FAIL,
  DELETE_PROJECT_TASK_SUCCESS,
  DELETE_PROJECT_TASK_REQUEST,
} from "../constants/ActionTypes";

export function* deleteProjectTask(action) {
  try {
    const response = yield call(
      deleteProjectTaskRequest,
      action.projectIdentifier,
      action.projectSequence
    );
    if (response.data.success) {
      yield put({
        type: DELETE_PROJECT_TASK_SUCCESS,
        projectSequence: action.projectSequence,
      });
    }
  } catch (error) {
    yield put({
      type: DELETE_PROJECT_TASK_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchDeleteProjectTask() {
  yield takeLatest(DELETE_PROJECT_TASK_REQUEST, deleteProjectTask);
}
