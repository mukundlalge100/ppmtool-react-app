import { put, takeLatest, call } from "redux-saga/effects";
import { getProjectTaskRequest } from "../api/BacklogApi";
import {
  GET_PROJECT_TASK_REQUEST,
  GET_PROJECT_TASK_SUCCESS,
  GET_PROJECT_TASK_FAIL,
} from "../constants/ActionTypes";

export function* getProjectTask(action) {
  try {
    const response = yield call(
      getProjectTaskRequest,
      action.projectIdentifier,
      action.projectSequence
    );
    yield put({ type: GET_PROJECT_TASK_SUCCESS, projectTask: response.data });
  } catch (error) {
    yield put({
      type: GET_PROJECT_TASK_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchGetProjectTask() {
  yield takeLatest(GET_PROJECT_TASK_REQUEST, getProjectTask);
}
