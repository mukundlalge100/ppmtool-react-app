import { put, takeLatest, call } from "redux-saga/effects";
import { updateProjectRequest } from "../api/ProjectApi";
import {
  UPDATE_PROJECT_REQUEST,
  UPDATE_PROJECT_SUCCESS,
  UPDATE_PROJECT_FAIL,
} from "../constants/ActionTypes";

export function* updateProject(action) {
  try {
    const response = yield call(
      updateProjectRequest,
      action.project,
      action.projectIdentifier
    );
    yield put({ type: UPDATE_PROJECT_SUCCESS, project: response.data });
    action.history.push("/");
  } catch (error) {
    yield put({
      type: UPDATE_PROJECT_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchUpdateProject() {
  yield takeLatest(UPDATE_PROJECT_REQUEST, updateProject);
}
