import { put, takeLatest } from "redux-saga/effects";
import {
  CLEAR_CREATE_PROJECT_ERRORS,
  CLEAR_CREATE_PROJECT_ERRORS_REQUEST,
} from "../constants/ActionTypes";

function* clearCreateProjectErrors() {
  yield put({ type: CLEAR_CREATE_PROJECT_ERRORS });
}

export default function* watchClearCreateProjectErrors() {
  yield takeLatest(
    CLEAR_CREATE_PROJECT_ERRORS_REQUEST,
    clearCreateProjectErrors
  );
}
