import { put, takeLatest } from "redux-saga/effects";
import {
  CLEAR_SIGNUP_ERRORS,
  CLEAR_SIGNUP_ERRORS_REQUEST,
} from "../constants/ActionTypes";

function* clearSignUpErrors() {
  yield put({ type: CLEAR_SIGNUP_ERRORS });
}

export default function* watchClearSignUpErrors() {
  yield takeLatest(CLEAR_SIGNUP_ERRORS_REQUEST, clearSignUpErrors);
}
