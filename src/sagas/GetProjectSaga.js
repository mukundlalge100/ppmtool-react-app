import { put, takeLatest, call } from "redux-saga/effects";
import { getProjectRequest } from "../api/ProjectApi";
import {
  GET_PROJECT_REQUEST,
  GET_PROJECT_SUCCESS,
  GET_PROJECT_FAIL,
} from "../constants/ActionTypes";

export function* getProject(action) {
  try {
    const response = yield call(getProjectRequest, action.projectIdentifier);
    yield put({ type: GET_PROJECT_SUCCESS, project: response.data });
  } catch (error) {
    yield put({
      type: GET_PROJECT_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchGetProject() {
  yield takeLatest(GET_PROJECT_REQUEST, getProject);
}
