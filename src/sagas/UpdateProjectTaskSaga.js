import { put, takeLatest, call } from "redux-saga/effects";
import { updateProjectTaskRequest } from "../api/BacklogApi";
import {
  UPDATE_PROJECT_TASK_REQUEST,
  UPDATE_PROJECT_TASK_SUCCESS,
  UPDATE_PROJECT_TASK_FAIL,
} from "../constants/ActionTypes";

export function* updateProjectTask(action) {
  try {
    const response = yield call(
      updateProjectTaskRequest,
      action.projectTask,
      action.projectIdentifier,
      action.projectSequence
    );
    yield put({
      type: UPDATE_PROJECT_TASK_SUCCESS,
      projectTask: response.data,
    });
    action.history.push(`/projectBoard/${action.projectIdentifier}`);
  } catch (error) {
    yield put({
      type: UPDATE_PROJECT_TASK_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchUpdateProjectTask() {
  yield takeLatest(UPDATE_PROJECT_TASK_REQUEST, updateProjectTask);
}
