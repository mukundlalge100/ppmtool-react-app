import { all, fork } from "redux-saga/effects";
import SignUpSaga from "./SignUpSaga";
import LogInSaga from "./LogInSaga";
import ClearSignUpErrorsSaga from "./ClearSignUpErrorsSaga";
import ClearLogInErrorsSaga from "./ClearLogInErrorsSaga";
import GetProjectsSaga from "./GetProjectsSaga";
import CheckLogInStateSaga from "./CheckLogInStateSaga";
import GetProjectTasksSaga from "./GetProjectTasksSaga";
import LogOutSaga from "./LogOutSaga";
import UpdateProjectSaga from "./UpdateProjectSaga";
import GetProjectSaga from "./GetProjectSaga";
import CreateProjectSaga from "./CreateProjectSaga";
import DeleteProjectSaga from "./DeleteProjectSaga";
import ClearProjectTaskErrorsSaga from "./ClearProjectTaskErrorsSaga";
import CreateProjectTaskSaga from "./CreateProjectTaskSaga";
import GetProjectTaskSaga from "./GetProjectTaskSaga";
import UpdateProjectTaskSaga from "./UpdateProjectTaskSaga";
import DeleteProjectTaskSaga from "./DeleteProjectTaskSaga";

function* rootSaga() {
  yield all([
    fork(LogInSaga),
    fork(LogOutSaga),
    fork(SignUpSaga),
    fork(ClearSignUpErrorsSaga),
    fork(ClearLogInErrorsSaga),
    fork(GetProjectsSaga),
    fork(GetProjectSaga),
    fork(CheckLogInStateSaga),
    fork(GetProjectTasksSaga),
    fork(UpdateProjectSaga),
    fork(CreateProjectSaga),
    fork(DeleteProjectSaga),
    fork(CreateProjectTaskSaga),
    fork(ClearProjectTaskErrorsSaga),
    fork(GetProjectTaskSaga),
    fork(UpdateProjectTaskSaga),
    fork(DeleteProjectTaskSaga),
  ]);
}

export default rootSaga;
