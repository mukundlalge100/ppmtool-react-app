import { put, takeLatest, call } from "redux-saga/effects";
import { getProjectsRequest } from "../api/ProjectApi";
import {
  GET_PROJECTS_REQUEST,
  GET_PROJECTS_SUCCESS,
  GET_PROJECTS_FAIL,
} from "../constants/ActionTypes";

export function* getProjects(action) {
  try {
    const response = yield call(getProjectsRequest, action.pageNumber);
    yield put({
      type: GET_PROJECTS_SUCCESS,
      projects: response.data.content,
      totalPages: response.data.totalPages,
      itemsCountPerPage: response.data.size,
      totalItemsCount: response.data.totalElements,
    });
  } catch (error) {
    yield put({
      type: GET_PROJECTS_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchGetProjects() {
  yield takeLatest(GET_PROJECTS_REQUEST, getProjects);
}
