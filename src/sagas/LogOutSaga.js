import { put, takeLatest, call } from "redux-saga/effects";
import { logOutRequest } from "../api/AuthApi";
import { setAuthToken } from "../utils/Util";
import {
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAIL,
} from "../constants/ActionTypes";

export function* logOut(action) {
  try {
    setAuthToken(false);
    localStorage.removeItem("jwtToken");
    yield call(logOutRequest);
    yield put({ type: LOGOUT_SUCCESS });
    action.history.push("/");
  } catch (error) {
    yield put({
      type: LOGOUT_FAIL,
      errors: error.response.data,
    });
  }
}

export default function* watchLogOut() {
  yield takeLatest(LOGOUT_REQUEST, logOut);
}
