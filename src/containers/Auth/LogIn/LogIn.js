import React, { Component } from "react";
import classes from "./LogIn.module.scss";
import utilClasses from "../../../utils/Util.module.scss";
import { Form, Field } from "react-final-form";
import Input from "../../../components/Input/Input";
import { Link } from "react-router-dom";
import logInValidations from "../../../validators/LogInValidations";
import { connect } from "react-redux";
import { logInRequest, clearLogInErrors } from "../../../actions/AuthActions";
import Loader from "../../../components/UI/Loader/Loader";
import PropTypes from "prop-types";
export class LogIn extends Component {
  state = {
    showHidePassword: false,
  };

  componentDidMount = () => {
    this.props.onClearLogInErrors();
    if (this.props.isAuthenticated) {
      this.props.history.push("/");
    }
  };
  componentDidUpdate = () => {
    if (this.state.showHidePassword) {
      setTimeout(() => {
        this.setState({ showHidePassword: false });
      }, 1000);
    }
  };

  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  logInFormSubmitHandler = (formValues) => {
    const userData = {
      email: formValues.email,
      password: formValues.password,
    };
    this.props.logInRequest(userData, this.props.history);
  };

  showHidePassword = () => {
    let passwordElement = document.getElementById("password");
    if (passwordElement.type === "password") {
      passwordElement.type = "text";
      setTimeout(() => {
        passwordElement.type = "password";
      }, 1000);
    }
    this.setState((prevState) => {
      return { showHidePassword: !prevState.showHidePassword };
    });
  };

  render() {
    const { isLoading, errors } = this.props;

    if (isLoading) {
      return (
        <div className={classes.LogIn}>
          <Loader />
        </div>
      );
    }
    return (
      <div className={classes.LogIn}>
        <h1
          className={`${utilClasses.Primary__Heading} ${utilClasses.Centered}`}
        >
          LogIn
        </h1>
        <Form
          onSubmit={this.logInFormSubmitHandler}
          validate={logInValidations}
          render={({
            handleSubmit,
            pristine,
            submitting,
            hasValidationErrors,
          }) => {
            return (
              <form onSubmit={handleSubmit} className={classes.LogIn_Form}>
                <Field
                  name="email"
                  component={Input}
                  label="Enter Your Email"
                  placeholder="Enter Your Email"
                  id="email"
                  info="Email address should contain '@' character."
                />
                <Field
                  component={Input}
                  id="password"
                  placeholder="Enter Your Password"
                  name="password"
                  label="Enter Your Password"
                  inputType="password"
                  showHidePassword={this.state.showHidePassword}
                  showHidePasswordFunc={this.showHidePassword}
                  error={errors.emailOrPasswordIsNotValid}
                />
                <button
                  className={utilClasses.Button}
                  style={{ justifySelf: "center" }}
                  disabled={pristine || submitting || hasValidationErrors}
                >
                  LogIn
                </button>
              </form>
            );
          }}
        />
        <p className={`${utilClasses.Paragraph}`}>
          Don't have an account ?{" "}
          <Link to="/signup" className={classes.LogInAndSignUp_Form__Link}>
            Create account here.
          </Link>
        </p>
      </div>
    );
  }
}

LogIn.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  errors: PropTypes.object,
  onClearLogInErrors: PropTypes.func.isRequired,
  logInRequest: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    isLoading: state.authReducer.isLoading,
    errors: state.authReducer.errors,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onClearLogInErrors: () => dispatch(clearLogInErrors()),
    logInRequest: (userData, history) =>
      dispatch(logInRequest(userData, history)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(LogIn);
