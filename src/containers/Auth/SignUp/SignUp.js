import { Field, Form } from "react-final-form";
import { connect } from "react-redux";
import React, { Component } from "react";
import utilClasses from "../../../utils/Util.module.scss";
import classes from "./SignUp.module.scss";
import Input from "../../../components/Input/Input";
import signUpValidations from "../../../validators/SignUpValidations";
import { signUpRequest, clearSignUpErrors } from "../../../actions/AuthActions";
import Loader from "../../../components/UI/Loader/Loader";

class SignUp extends Component {
  state = {
    showHideConfirmPassword: false,
    showHidePassword: false,
  };
  componentDidMount = () => {
    this.props.clearSignUpErrors();
  };

  componentDidUpdate = () => {
    if (this.state.showHidePassword) {
      setTimeout(() => {
        this.setState({ showHidePassword: false });
      }, 1000);
    }
    if (this.state.showHideConfirmPassword) {
      setTimeout(() => {
        this.setState({ showHideConfirmPassword: false });
      }, 1000);
    }
  };

  signUpFormSubmitHandler = (formValues) => {
    this.props.signUpRequest(formValues, this.props.history);
  };

  showHidePassword = () => {
    let passwordElement = document.getElementById("password");
    if (passwordElement.type === "password") {
      passwordElement.type = "text";
      setTimeout(() => {
        passwordElement.type = "password";
      }, 1000);
    }
    this.setState((prevState) => {
      return { showHidePassword: !prevState.showHidePassword };
    });
  };

  showHideConfirmPassword = () => {
    let passwordElement = document.getElementById("confirmPassword");
    if (passwordElement.type === "password") {
      passwordElement.type = "text";
      setTimeout(() => {
        passwordElement.type = "password";
      }, 1000);
    } else {
      passwordElement.type = "password";
    }
    this.setState((prevState) => {
      return { showHideConfirmPassword: !prevState.showHideConfirmPassword };
    });
  };
  render() {
    const { isLoading, errors } = this.props;

    if (isLoading) {
      return (
        <div className={classes.SignUp}>
          <Loader />
        </div>
      );
    }
    return (
      <div className={classes.SignUp}>
        <h1
          className={utilClasses.Primary__Heading}
          style={{ justifySelf: "center" }}
        >
          Create Your account
        </h1>
        <Form
          onSubmit={this.signUpFormSubmitHandler}
          validate={signUpValidations}
          render={({
            pristine,
            submitting,
            handleSubmit,
            hasValidationErrors,
          }) => (
            <form onSubmit={handleSubmit} className={classes.SignUp_Form}>
              <Field
                component={Input}
                name="userName"
                id="userName"
                placeholder="* UserName"
                label="* UserName"
                info="Please provide your user name."
              />
              <Field
                component={Input}
                label="Email Address"
                name="email"
                id="email"
                placeholder="* Email"
                inputType="email"
                info="Please provide your email address.Email should contain '@' character."
                error={errors.email}
              />
              <Field
                component={Input}
                name="password"
                placeholder="* Password"
                id="password"
                inputType="password"
                label="Password"
                info="Password should be minimum 8 characters and maximum 64 character long."
                error={errors.password}
                showHidePassword={this.state.showHidePassword}
                showHidePasswordFunc={this.showHidePassword}
              />
              <Field
                component={Input}
                name="confirmPassword"
                placeholder="* Confirm Password"
                id="confirmPassword"
                inputType="password"
                label="Confirm Password"
                info="Please confirm your password"
                showHideConfirmPassword={this.state.showHideConfirmPassword}
                showHideConfirmPasswordFunc={this.showHideConfirmPassword}
              />
              <button
                style={{ justifySelf: "center", margin: "3rem 0rem" }}
                className={utilClasses.Button}
                disabled={submitting || pristine || hasValidationErrors}
              >
                SignUp
              </button>
            </form>
          )}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.signUpReducer.isLoading,
    errors: state.signUpReducer.errors,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    signUpRequest: (userData, history) =>
      dispatch(signUpRequest(userData, history)),
    clearSignUpErrors: () => dispatch(clearSignUpErrors()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
