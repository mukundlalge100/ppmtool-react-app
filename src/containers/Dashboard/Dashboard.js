import React, { Component } from "react";
import classes from "./Dashboard.module.scss";
import utilClasses from "../../utils/Util.module.scss";
import { Link } from "react-router-dom";
import Projects from "../Projects/Projects";

class Dashboard extends Component {
  render() {
    return (
      <div className={classes.Dashboard}>
        <header className={classes.Dashboard_Header}>
          <Link
            to="/addProject"
            className={`${classes.Dashboard_Header__Button} ${utilClasses.Button}`}
          >
            Create Project
          </Link>
          <hr />
          <h1
            className={utilClasses.Primary__Heading}
            style={{ justifySelf: "center" }}
          >
            Welcome to Personal Project Manager Tool
          </h1>
        </header>
        <Projects />
      </div>
    );
  }
}
export default Dashboard;
