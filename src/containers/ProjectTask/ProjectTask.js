import React, { Component } from "react";
import classes from "./ProjectTask.module.scss";
import PropTypes from "prop-types";
import utilClasses from "../../utils/Util.module.scss";
import { Link } from "react-router-dom";

class ProjectTask extends Component {
  render() {
    const { projectTask, projectIdentifier } = this.props;
    let priority;

    if (projectTask.priority === 1) {
      priority = "HIGH";
    } else if (projectTask.priority === 2) {
      priority = "MEDIUM";
    } else if (projectTask.priority === 3) {
      priority = "LOW";
    }
    return (
      <article className={classes.ProjectTask}>
        <div
          className={`${classes.ProjectTask_Heading} ${
            priority === "LOW"
              ? classes.ProjectTask_Low
              : priority === "MEDIUM"
              ? classes.ProjectTask_Medium
              : classes.ProjectTask_High
          }`}
        >
          <span className={utilClasses.Paragraph}>
            Task ID : {projectTask.projectSequence} &rArr;{" "}
          </span>
          <strong className={utilClasses.Paragraph}>
            Priority : {priority}
          </strong>
        </div>
        <div className={classes.ProjectTask_Content}>
          <strong className={classes.ProjectTask_Content__Summary}>
            {" "}
            {projectTask.summary}
          </strong>
          <span className={utilClasses.Paragraph}>
            {projectTask.acceptenceCriteria}
          </span>
          <div className={classes.ProjectTask_Content__ButtonContainer}>
            <Link
              to={`/projectBoard/${projectIdentifier}/${projectTask.projectSequence}`}
              className={utilClasses.Button}
            >
              View/Update Task
            </Link>
            <button
              onClick={() =>
                this.props.onDeleteProjectTask(
                  projectIdentifier,
                  projectTask.projectSequence
                )
              }
              className={`${utilClasses.Button} ${utilClasses.MB__Small}`}
            >
              Delete Task
            </button>
          </div>
        </div>
      </article>
    );
  }
}

ProjectTask.propTypes = {
  projectTask: PropTypes.object.isRequired,
};

export default ProjectTask;
