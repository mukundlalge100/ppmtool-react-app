import React, { Component } from "react";
import { Link } from "react-router-dom";
import classes from "./AddProjectTask.module.scss";
import utilClasses from "../../../utils/Util.module.scss";
import { Field, Form } from "react-final-form";
import { connect } from "react-redux";
import Input from "../../../components/Input/Input";
import TextAreaField from "../../../components/Input/TextAreaField/TextAreaField";
import SelectField from "../../../components/Input/SelectField/SelectField";

import addProjectTaskValidations from "../../../validators/AddProjectTaskValidations";
import Loader from "../../../components/UI/Loader/Loader";
import PropTypes from "prop-types";

import {
  createProjectTask,
  clearErrors,
} from "../../../actions/BacklogActions";

const priorityOptions = [
  { value: "", label: "Select Priority" },
  { value: 3, label: "LOW" },
  { value: 2, label: "MEDIUM" },
  { value: 1, label: "HIGH" },
];
const statusOptions = [
  { value: "", label: "Select Status" },
  { value: "TO_DO", label: "TO_DO" },
  { value: "IN_PROGRESS", label: "IN_PROGRESS" },
  { value: "DONE", label: "DONE" },
];
class AddProjectTask extends Component {
  componentDidMount = () => {
    this.props.onClearErrors();
  };
  handleSubmit = (formValues) => {
    this.props.onCreateProjectTask(
      formValues,
      this.props.match.params.projectIdentifier,
      this.props.history
    );
  };

  render() {
    const { errors, isLoading } = this.props;

    if (isLoading) {
      return (
        <div className={classes.AddProjectTask}>
          <Loader />
        </div>
      );
    }
    return (
      <div className={classes.AddProjectTask}>
        <Link
          to={`/projectBoard/${this.props.match.params.projectIdentifier}`}
          className={utilClasses.Link}
        >
          &lArr; Back
        </Link>
        <h1
          className={utilClasses.Primary__Heading}
          style={{ justifySelf: "center" }}
        >
          Create Project Task Form
        </h1>
        <Form
          onSubmit={this.handleSubmit}
          validate={addProjectTaskValidations}
          render={({ handleSubmit, pristine, submitting }) => (
            <form
              onSubmit={handleSubmit}
              className={classes.AddProjectTask_Form}
            >
              <Field
                component={Input}
                name="summary"
                id="summary"
                placeholder="* Project Summary"
                label="* Project Summary"
                info="Give summary for project task"
                error={errors.summary || errors.projectNotFound}
              />
              <Field
                component={TextAreaField}
                name="acceptenceCriteria"
                id="acceptenceCriteria"
                placeholder="* Acceptence Criteria"
                label="* Acceptence Criteria"
                info="Acceptence criteria for project task"
                error={errors.acceptenceCriteria}
              />
              <Field
                component={Input}
                label="Due Date"
                name="dueDate"
                id="dueDate"
                inputType="date"
                info="Due date for project task"
              />
              <Field
                component={SelectField}
                name="priority"
                label="Priority of project task"
                options={priorityOptions}
                id="priority"
                info="Select priority for project task ,default is 3 (low)"
              />
              <Field
                component={SelectField}
                name="status"
                label="Project Task Status"
                options={statusOptions}
                id="status"
                info="Select Status for project task ,default is 'TO_DO'"
              />
              <button
                style={{ justifySelf: "center", margin: "3rem 0rem" }}
                className={utilClasses.Button}
                disabled={submitting || pristine}
              >
                Add Project
              </button>
            </form>
          )}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    errors: state.projectTaskReducer.errors,
    isLoading: state.projectTaskReducer.isLoading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onCreateProjectTask: (task, projectIdenitifier, history) =>
      dispatch(createProjectTask(task, projectIdenitifier, history)),
    onClearErrors: () => dispatch(clearErrors()),
  };
};

AddProjectTask.propTypes = {
  errors: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onClearErrors: PropTypes.func.isRequired,
  onCreateProjectTask: PropTypes.func.isRequired,
};
export default AddProjectTask = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProjectTask);
