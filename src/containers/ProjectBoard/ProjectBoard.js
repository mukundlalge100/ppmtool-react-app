import React, { Component } from "react";
import classes from "./ProjectBoard.module.scss";
import { Link } from "react-router-dom";
import utilClasses from "../../utils/Util.module.scss";
import { isEmpty } from "../../utils/Util";
import Loader from "../../components/UI/Loader/Loader";

import { connect } from "react-redux";
import ProjectTask from "../ProjectTask/ProjectTask";
import ErrorBoundary from "../ErrorBoundary/ErrorBoundary";

import {
  getProjectTasks,
  clearErrors,
  deleteProjectTask,
} from "../../actions/BacklogActions";

class ProjectBoard extends Component {
  componentDidMount = () => {
    this.props.onGetProjectTasks(this.props.match.params.projectIdentifier);
  };

  componentWillUnmount = () => {
    // this.props.onClearErrors();
  };
  deleteProjectTask = (projectIdentifier, projectSequence) => {
    if (window.confirm("Are you sure to delete project task?")) {
      this.props.onDeleteProjectTask(projectIdentifier, projectSequence);
    }
  };

  render() {
    const { isLoading, projectTasks, errors, match } = this.props;
    const { projectIdentifier } = match.params;
    let toDoItems = [],
      inProgressItems = [],
      doneItems = [];

    const tasks = projectTasks.map((projectTask) => {
      return (
        <ProjectTask
          onDeleteProjectTask={this.deleteProjectTask}
          projectIdentifier={projectIdentifier}
          key={projectTask.projectSequence}
          projectTask={projectTask}
        />
      );
    });

    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].props.projectTask.status === "TO_DO") {
        toDoItems.push(tasks[i]);
      } else if (tasks[i].props.projectTask.status === "IN_PROGRESS") {
        inProgressItems.push(tasks[i]);
      } else if (tasks[i].props.projectTask.status === "DONE") {
        doneItems.push(tasks[i]);
      }
    }

    // SHOW LOADER WHEN BACKLOG LOADING IS TRUE ...
    if (isLoading) {
      return (
        <div className={utilClasses.Loader__Centered}>
          <Loader />
        </div>
      );
    }
    let projectBoard;

    if (isEmpty(projectTasks)) {
      if (!isEmpty(errors)) {
        projectBoard = <ErrorBoundary message={errors.projectNotFound} />;
      } else {
        projectBoard = (
          <ErrorBoundary
            message={`
              No Tasks found for Project with ID ${projectIdentifier}`}
          />
        );
      }
    } else {
      projectBoard = (
        <React.Fragment>
          <h2
            style={{ textAlign: "center" }}
            className={utilClasses.Secondary__Heading}
          >
            Project Tasks
          </h2>
          <div className={classes.ProjectBoard_StatusContainer}>
            <div className={classes.ProjectBoard_Status}>
              <div className={classes.ProjectBoard_Status__ToDo}>
                <p className={classes.ProjectBoard_Status__Text}>TO_DO</p>
              </div>
              {toDoItems}
            </div>
            <div className={classes.ProjectBoard_Status}>
              <div className={classes.ProjectBoard_Status__InProgress}>
                <p className={classes.ProjectBoard_Status__Text}>IN_PROGRESS</p>
              </div>
              {inProgressItems}
            </div>
            <div className={classes.ProjectBoard_Status}>
              <div className={classes.ProjectBoard_Status__Done}>
                <p className={classes.ProjectBoard_Status__Text}>DONE</p>
              </div>
              {doneItems}
            </div>
          </div>
        </React.Fragment>
      );
    }

    return (
      <div className={classes.ProjectBoard}>
        <Link
          to={`/addProjectTask/${projectIdentifier}`}
          className={`${utilClasses.Button} ${classes.ProjectBoard_Button}`}
        >
          Create ProjectTask
        </Link>
        <hr style={{ width: "100%" }} />
        {projectBoard}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    projectTasks: state.projectTaskReducer.projectTasks,
    errors: state.projectTaskReducer.errors,
    isLoading: state.projectTaskReducer.isLoading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onGetProjectTasks: (projectIdentifier) =>
      dispatch(getProjectTasks(projectIdentifier)),
    onDeleteProjectTask: (projectIdentifier, projectSequence) =>
      dispatch(deleteProjectTask(projectIdentifier, projectSequence)),
    onClearErrors: () => dispatch(clearErrors),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectBoard);
