/* eslint-disable no-restricted-globals */
import React, { Component } from "react";
import { randomColor, randomIntFromRange } from "../../utils/Util";
import Circle from "./Circle";
import classes from "./Canvas.module.scss";

class Canvas extends Component {
  constructor(props) {
    super(props);
    this.canvasRef = React.createRef();
  }
  componentDidMount = () => {
    this.canvas = this.canvasRef.current;
    this.c = this.canvas.getContext("2d");
    this.canvas.width = innerWidth - 25;
    this.canvas.height = innerHeight;
    this.mouse = {
      x: -40,
      y: -40
    };
    this.init();
    this.animate(this.canvas, this.c);
    addEventListener("resize", () => {
      this.canvas.width = innerWidth - 25;
      this.canvas.height = innerHeight;
      this.init();
    });
  };

  init() {
    const colors = ["#402837", "#090426", "#F2EAC2", "#BF9D7E", "#73534C"];
    this.circles = [];
    for (let i = 0; i < 1000; i++) {
      const radius = randomIntFromRange(1, 5);
      const x = randomIntFromRange(0, innerWidth - radius * 2 + radius);
      const y = randomIntFromRange(0, innerHeight - radius * 2 + radius);
      const dy = Math.random() * 0.5;
      const dx = Math.random() * 0.5;
      const color = randomColor(colors);
      this.circles.push(
        new Circle(x, y, dx, dy, radius, color, this.c, this.canvas, this.mouse)
      );
    }
  }
  animate(canvas, c) {
    requestAnimationFrame(() => this.animate(canvas, c));
    c.clearRect(0, 0, canvas.width, canvas.height);
    this.circles.forEach(circle => {
      circle.update();
    });
  }
  mouseMove = event => {
    this.mouse.x = event.nativeEvent.clientX;
    this.mouse.y = event.nativeEvent.clientY;
  };
  onClick = event => {
    this.canvas.width = innerWidth;
    this.canvas.height = innerHeight;
    this.init();
  };
  mouseOut = event => {
    this.mouse.x = -40;
    this.mouse.y = -40;
  };
  render() {
    return (
      <canvas
        ref={this.canvasRef}
        onMouseOut={event => this.mouseOut(event)}
        onMouseMove={event => this.mouseMove(event)}
        onClick={this.onClick}
        className={classes.Canvas}
      ></canvas>
    );
  }
}

export default Canvas;
