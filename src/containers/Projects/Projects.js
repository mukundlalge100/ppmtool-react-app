import React, { Component } from "react";
import classes from "./Projects.module.scss";
import PropTypes from "prop-types";
import Project from "../Project/Project";
import { connect } from "react-redux";
import { getProjects } from "../../actions/ProjectActions";
import Loader from "../../components/UI/Loader/Loader";
import utilClasses from "../../utils/Util.module.scss";
import Pagination from "react-js-pagination";
import Modal from "../Modal/Modal";
import { isEmpty } from "../../utils/Util";

class Projects extends Component {
  state = {
    currentPage: 1,
    showModal: true,
  };

  componentDidMount = () => {
    this.props.getProjects(this.state.currentPage - 1);
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.currentPage !== this.state.currentPage) {
      this.props.getProjects(this.state.currentPage - 1);
    }
  };

  changePageHandler = (currentPage) => {
    this.setState({ currentPage });
  };

  showModalToggleHandler = () => {
    this.setState((prevState) => ({ showModal: !prevState.showModal }));
  };

  render() {
    const { isLoading, projects } = this.props;

    if (isLoading) {
      return (
        <div className={classes.Projects}>
          <Loader />
        </div>
      );
    }
    if (isEmpty(projects)) {
      return (
        <Modal
          show={this.state.showModal}
          closeModal={this.showModalToggleHandler}
        >
          <h1 className={utilClasses.Primary__Heading}>No projects found</h1>
        </Modal>
      );
    }
    const renderProjects = projects.map((project) => {
      return (
        <Project
          totalPages={this.props.totalPages}
          currentPage={this.state.currentPage}
          key={project.projectIdentifier}
          project={project}
          onChangePageHandler={this.changePageHandler}
        />
      );
    });
    return (
      <div className={classes.Projects}>
        {renderProjects}
        {this.props.totalItemsCount > 4 ? (
          <Pagination
            pageRangeDisplayed={3}
            onChange={this.changePageHandler}
            innerClass={utilClasses.Pagination}
            itemsCountPerPage={this.props.itemsCountPerPage}
            activePage={this.state.currentPage}
            totalItemsCount={this.props.totalItemsCount}
            activeLinkClass={utilClasses.Pagination_LinkContainer__Link_Active}
            activeClass={utilClasses.Pagination_LinkContainer__Active}
            itemClass={utilClasses.Pagination_LinkContainer}
            linkClass={utilClasses.Pagination_LinkContainer__Link}
          />
        ) : null}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.projectReducer.isLoading,
    projects: state.projectReducer.projects,
    totalPages: state.projectReducer.totalPages,
    itemsCountPerPage: state.projectReducer.itemsCountPerPage,
    totalItemsCount: state.projectReducer.totalItemsCount,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getProjects: (pageNumber) => dispatch(getProjects(pageNumber)),
  };
};

export default Projects = connect(
  mapStateToProps,
  mapDispatchToProps
)(Projects);
