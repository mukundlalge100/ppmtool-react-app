import PropTypes from "prop-types";
import React, { Component } from "react";
import { Field, Form } from "react-final-form";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Input from "../../../components/Input/Input";
import TextAreaField from "../../../components/Input/TextAreaField/TextAreaField";
import Loader from "../../../components/UI/Loader/Loader";
import {
  createProject,
  clearCreateProjectErrors,
} from "../../../actions/ProjectActions";
import utilClasses from "../../../utils/Util.module.scss";
import addProjectValidations from "../../../validators/AddProjectValidations";
import classes from "./AddProject.module.scss";

class AddProject extends Component {
  state = {
    validating: false,
  };
  componentDidMount = () => {
    this.props.onClearErrors();
  };
  handleSubmit = (formValues) => {
    this.props.onCreateProject(formValues, this.props.history);
  };

  render() {
    const { errors, isLoading } = this.props;

    if (isLoading) {
      return (
        <div className={classes.AddProject}>
          <Loader />
        </div>
      );
    }
    return (
      <div className={classes.AddProject}>
        <Link to="/" className={utilClasses.Link}>
          &lArr; Back
        </Link>
        <h1
          className={utilClasses.Primary__Heading}
          style={{ justifySelf: "center" }}
        >
          Create Project Form
        </h1>
        <Form
          onSubmit={this.handleSubmit}
          validate={addProjectValidations}
          render={({ handleSubmit, pristine, submitting, validating }) => (
            <form onSubmit={handleSubmit} className={classes.AddProject_Form}>
              <Field
                component={Input}
                name="projectName"
                id="projectName"
                placeholder="* Project Name"
                label="* Project Name"
                info="Give a nice name to your project"
                error={errors.projectName}
              />
              <Field
                component={Input}
                name="projectIdentifier"
                id="projectIdentifier"
                validating={validating}
                placeholder="* Project Identifier"
                label="* Project Identifier"
                info="Project ID should be between 4-5 characters"
                error={errors.projectIdentifier}
              />
              <Field
                component={TextAreaField}
                name="description"
                id="description"
                placeholder="* Project Description"
                label="* Project Description"
                info="Give some information about your project"
                error={errors.description}
              />
              <Field
                component={Input}
                label="Start Date"
                name="startDate"
                id="startDate"
                inputType="date"
                info="Start date of your project"
              />
              <Field
                component={Input}
                name="endDate"
                label="End Date"
                id="endDate"
                inputType="date"
                info="Estimated end date of your date"
              />
              <button
                style={{ justifySelf: "center", margin: "3rem 0rem" }}
                className={utilClasses.Button}
                disabled={submitting || pristine}
              >
                Add Project
              </button>
            </form>
          )}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    errors: state.projectReducer.errors,
    isLoading: state.projectReducer.isLoading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onCreateProject: (project, history) =>
      dispatch(createProject(project, history)),
    onClearErrors: () => dispatch(clearCreateProjectErrors()),
  };
};

AddProject.propTypes = {
  errors: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onClearErrors: PropTypes.func.isRequired,
  onCreateProject: PropTypes.func.isRequired,
};
export default AddProject = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProject);
