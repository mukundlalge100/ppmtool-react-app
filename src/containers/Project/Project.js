import React, { Component } from "react";
import classes from "./Project.module.scss";
import utilClasses from "../../utils/Util.module.scss";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import { ReactComponent as DeleteSVG } from "../../assets/SVG/trash.svg";
import { ReactComponent as UpdateSVG } from "../../assets/SVG/arrow-repeat-outline.svg";
import { ReactComponent as ProjectBoardSVG } from "../../assets/SVG/flag.svg";
import { formatDate } from "../../utils/Util";
import { connect } from "react-redux";
import { deleteProject } from "../../actions/ProjectActions";

class Project extends Component {
  deleteProject = (projectIdentifier, currentPage) => {
    if (
      window.confirm(
        "Are you sure? This action will delete project and its associated details !"
      )
    ) {
      if (this.props.projects.length === 1) {
        if (this.props.totalPages === 1) {
          this.props.onChangePageHandler(currentPage);
          this.props.onDeleteProject(projectIdentifier, 0);
          return;
        }
        this.props.onChangePageHandler(currentPage - 1);
        this.props.onDeleteProject(projectIdentifier, currentPage - 2);
      } else {
        this.props.onDeleteProject(projectIdentifier, currentPage - 1);
      }
    }
  };
  render() {
    const { project } = this.props;
    return (
      <div className={classes.Project}>
        <div className={classes.Project_Info}>
          <p className={classes.Project_Info__Identifier}>
            {project.projectIdentifier}
          </p>
          <div className={classes.Project_Info__Container}>
            <p style={{ fontSize: "1.6rem", fontWeight: "bolder" }}>
              PROJECT NAME :- {project.projectName.toUpperCase()}
            </p>
            <p>
              <b>DESCRIPTION :- </b>
              <br />
              {project.description}
            </p>
            <p>
              <b>START DATE :- </b>
              <br />
              {formatDate(project.startDate)}
            </p>
          </div>
        </div>
        <div className={classes.Project_Actions}>
          <Link
            to={`/projectBoard/${project.projectIdentifier}`}
            className={`${utilClasses.Button} ${classes.Project_Actions__Button}`}
          >
            <ProjectBoardSVG className={classes.Project_Actions__SVG} />
            Project Board
          </Link>
          <Link
            to={`/updateProject/${project.projectIdentifier}`}
            className={`${utilClasses.Button} ${classes.Project_Actions__Button}`}
          >
            <UpdateSVG className={classes.Project_Actions__SVG} />
            Update Project
          </Link>
          <Link
            to=""
            className={`${utilClasses.Button} ${classes.Project_Actions__Button}`}
            onClick={() =>
              this.deleteProject(
                project.projectIdentifier,
                this.props.currentPage
              )
            }
          >
            <DeleteSVG className={classes.Project_Actions__SVG} />
            Delete Project
          </Link>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    projects: state.projectReducer.projects,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteProject: (projectIdentifier, currentPage) =>
      dispatch(deleteProject(projectIdentifier, currentPage)),
  };
};

Project.propTypes = {
  project: PropTypes.object.isRequired,
  currentPage: PropTypes.number.isRequired,
  onDeleteProject: PropTypes.func.isRequired,
  projects: PropTypes.array.isRequired,
  onChangePageHandler: PropTypes.func.isRequired,
};

export default Project = connect(mapStateToProps, mapDispatchToProps)(Project);
