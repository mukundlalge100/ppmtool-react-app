import React, { Component } from "react";
import classes from "./EditProject.module.scss";
import { Link } from "react-router-dom";
import utilClasses from "../../../utils/Util.module.scss";
import { Field, Form } from "react-final-form";
import { connect } from "react-redux";
import Input from "../../../components/Input/Input";
import TextAreaField from "../../../components/Input/TextAreaField/TextAreaField";
import PropTypes from "prop-types";
import updateProjectValidations from "../../../validators/UpdateProjectValidations";
import Loader from "../../../components/UI/Loader/Loader";

import { updateProject, getProject } from "../../../actions/ProjectActions";
import { formatDate, isEmpty } from "../../../utils/Util";

class EditProject extends Component {
  componentDidMount = () => {
    if (this.props.match.params) {
      this.props.onGetProject(this.props.match.params.projectId);
    }
  };
  handleSubmit = (formValues) => {
    const projectIdentifier = this.props.match.params.projectId;
    formValues.id = this.props.project.id;
    this.props.onUpdateProject(
      formValues,
      projectIdentifier,
      this.props.history
    );
  };

  render() {
    const { errors, initialValues, isLoading } = this.props;

    if (isLoading) {
      return (
        <div className={utilClasses.Loader__Centered}>
          <Loader />
        </div>
      );
    }
    return (
      <div className={classes.EditProject}>
        <Link to="/" className={utilClasses.Link}>
          &lArr; Back
        </Link>
        <h1
          className={utilClasses.Primary__Heading}
          style={{ justifySelf: "center" }}
        >
          Update Project Form
        </h1>
        <Form
          initialValues={initialValues}
          onSubmit={this.handleSubmit}
          validate={updateProjectValidations}
          render={({ submitting, pristine, handleSubmit, form }) => (
            <form onSubmit={handleSubmit} className={classes.EditProject_Form}>
              <Field
                component={Input}
                name="projectName"
                id="projectName"
                placeholder="Project Name"
                label="Project Name"
                info="Give a nice name to your project"
                error={errors.projectName}
              />
              <Field
                component={Input}
                name="projectIdentifier"
                id="projectIdentifier"
                placeholder="Project Identifier"
                label="Project Identifier"
                info="Project ID should be between 4-5 characters"
                error={errors.projectIdentifier}
                disabled={true}
              />
              <Field
                component={TextAreaField}
                name="description"
                id="description"
                placeholder="Project Description"
                label="Project Description"
                info="Give some information about your project"
                error={errors.description}
              />
              <Field
                component={Input}
                label="Start Date (dd-mm-yyyy)"
                name="startDate"
                id="startDate"
                inputType="date"
                info="Start date of your project"
                error={errors.startDate}
              />
              <Field
                component={Input}
                name="endDate"
                label="End Date (dd-mm-yyyy)"
                id="endDate"
                inputType="date"
                info="Estimated end date of your project"
              />
              <button
                style={{ justifySelf: "center", margin: "3rem 0rem" }}
                className={utilClasses.Button}
                disabled={pristine || submitting}
              >
                Update Project
              </button>
            </form>
          )}
        ></Form>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  let initialValues = null;
  if (state.projectReducer.project) {
    let startDate = null;
    let endDate = null;
    if (!isEmpty(state.projectReducer.project.startDate)) {
      startDate = formatDate(state.projectReducer.project.startDate);
    }
    if (!isEmpty(state.projectReducer.project.endDate)) {
      endDate = formatDate(state.projectReducer.project.endDate);
    }
    initialValues = {
      projectIdentifier: state.projectReducer.project.projectIdentifier,
      projectName: state.projectReducer.project.projectName,
      description: state.projectReducer.project.description,
      startDate,
      endDate,
    };
  }
  return {
    initialValues: initialValues,
    errors: state.projectReducer.errors,
    project: state.projectReducer.project,
    isLoading: state.projectReducer.isLoading || state.projectReducer.isLoading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onUpdateProject: (project, projectIdentifier, history) =>
      dispatch(updateProject(project, projectIdentifier, history)),
    onGetProject: (projectId) => dispatch(getProject(projectId)),
  };
};

EditProject.propTypes = {
  errors: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onUpdateProject: PropTypes.func.isRequired,
  project: PropTypes.object,
};

export default EditProject = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProject);
