import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";
import { ReactComponent as UserSVG } from "../../../assets/SVG/user.svg";
import { ReactComponent as DashBoardSVG } from "../../../assets/SVG/home.svg";
import { ReactComponent as LogInSVG } from "../../../assets/SVG/login.svg";
import { ReactComponent as Brand } from "../../../assets/SVG/PPMTool.svg";
import { ReactComponent as LogOutSVG } from "../../../assets/SVG/unlocked.svg";
import { ReactComponent as SignUpSVG } from "../../../assets/SVG/sign-up-key.svg";
import utilClasses from "../../../utils/Util.module.scss";
import classes from "./Navbar.module.scss";
import SideDrawerToggle from "../../../components/UI/SideDrawer/SideDrawerToggle/SideDrawerToggle";
import SideDrawer from "../../../components/UI/SideDrawer/SideDrawer";
import { connect } from "react-redux";
import { logOutRequest } from "../../../actions/AuthActions";
import { withRouter } from "react-router-dom";

class Navbar extends Component {
  state = {
    showHideSideDrawer: false,
  };
  authLogOut = () => {
    this.sideDrawerCloseHandler();
    this.props.onAuthLogOut(this.props.history);
  };
  showHideSideDrawer = () => {
    this.setState((prevState) => {
      return {
        showHideSideDrawer: !prevState.showHideSideDrawer,
      };
    });
  };
  sideDrawerCloseHandler = () => {
    this.setState({ showHideSideDrawer: false });
  };

  authLogOut = () => {
    this.props.onAuthLogOut(this.props.history);
    this.sideDrawerCloseHandler();
  };

  render() {
    let conditionalRoutes;
    const { isAuthenticated, user } = this.props;
    if (isAuthenticated) {
      conditionalRoutes = (
        <nav
          className={`${classes.Navbar_Nav} ${
            this.state.showHideSideDrawer
              ? classes.Navbar_SideDrawerShow
              : classes.Navbar_SideDrawerHide
          }`}
        >
          <NavLink
            to="/"
            activeClassName={classes.Navbar_Links__Link_Active}
            className={classes.Navbar_Brand}
            onClick={this.sideDrawerCloseHandler}
          >
            <Brand className={classes.Navbar_Brand__SVG} />
            <h3 className={utilClasses.Tertiary__Heading}>
              Personal Project Management Tool
            </h3>
          </NavLink>
          <div className={classes.Navbar_Links}>
            <NavLink
              to="/dashboard"
              activeClassName={classes.Navbar_Links__Link_Active}
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <DashBoardSVG className={classes.Navbar_Links__Link_SVG} />
              <p>Dashboard</p>
            </NavLink>

            <NavLink
              to="#"
              activeClassName={classes.Navbar_Links__Link_Active}
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <UserSVG className={classes.Navbar_Links__Link_SVG} />
              <p>{user.userName}</p>
            </NavLink>
            <div
              className={classes.Navbar_Links__Link}
              onClick={this.authLogOut}
            >
              <LogOutSVG className={classes.Navbar_Links__Link_SVG} />
              <p>LogOut</p>
            </div>
          </div>
        </nav>
      );
    } else {
      conditionalRoutes = (
        <nav
          className={`${classes.Navbar_Nav} ${
            this.state.showHideSideDrawer
              ? classes.Navbar_SideDrawerShow
              : classes.Navbar_SideDrawerHide
          }`}
        >
          <NavLink
            to="/"
            activeClassName={classes.Navbar_Links__Link_Active}
            className={classes.Navbar_Brand}
            onClick={this.sideDrawerCloseHandler}
          >
            <Brand className={classes.Navbar_Brand__SVG} />
            <h3 className={utilClasses.Tertiary__Heading}>
              Personal Project Management Tool
            </h3>
          </NavLink>
          <div className={classes.Navbar_Links}>
            <NavLink
              to="/"
              activeClassName={classes.Navbar_Links__Link_Active}
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <DashBoardSVG className={classes.Navbar_Links__Link_SVG} />
              <p>Dashboard</p>
            </NavLink>
            <Link
              to="/signup"
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <SignUpSVG className={classes.Navbar_Links__Link_SVG} />
              <p>SignUp</p>
            </Link>
            <Link
              to="/login"
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <LogInSVG className={classes.Navbar_Links__Link_SVG} />
              <p>LogIn</p>
            </Link>
          </div>
        </nav>
      );
    }

    return (
      <div className={classes.Navbar}>
        <SideDrawerToggle sideDrawerToggle={this.showHideSideDrawer} />

        {this.state.showHideSideDrawer ? (
          <SideDrawer
            showHideSideDrawer={this.state.showHideSideDrawer}
            sideDrawerCloseHandler={this.sideDrawerCloseHandler}
          >
            {conditionalRoutes}
          </SideDrawer>
        ) : (
          conditionalRoutes
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    user: state.authReducer.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onAuthLogOut: (history) => dispatch(logOutRequest(history)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navbar));
