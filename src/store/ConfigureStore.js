import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas/rootSaga";
import rootReducer from "../reducers/rootReducer";
const sagaMiddleware = createSagaMiddleware();
import { createLogger } from "redux-logger";

const logger = createLogger({
  duration: true,
  timestamp: true,
  logErrors: true,
  diff: true,
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let store = {};
if (process.env.NODE_ENV === "production") {
  store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  );
} else {
  store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware, logger))
  );
}

sagaMiddleware.run(rootSaga);

export default store;
