import {
  LOGIN_REQUEST,
  SIGNUP_REQUEST,
  LOGIN_SUCCESS,
  CLEAR_LOGIN_ERRORS_REQUEST,
  CLEAR_SIGNUP_ERRORS_REQUEST,
  LOGOUT_REQUEST,
  CHECK_LOGIN_STATE_REQUEST,
} from "../constants/ActionTypes";

export const logInRequest = (userData, history) => {
  return {
    type: LOGIN_REQUEST,
    userData,
    history,
  };
};
export const signUpRequest = (userData, history) => {
  return {
    type: SIGNUP_REQUEST,
    userData,
    history,
  };
};

export const logInSuccess = (user) => {
  return {
    type: LOGIN_SUCCESS,
    user,
  };
};

export const logOutRequest = (history) => {
  return {
    type: LOGOUT_REQUEST,
    history,
  };
};

export const checkLogInStateRequest = (history) => {
  return {
    type: CHECK_LOGIN_STATE_REQUEST,
    history,
  };
};

export const clearLogInErrors = () => {
  return {
    type: CLEAR_LOGIN_ERRORS_REQUEST,
  };
};

export const clearSignUpErrors = () => {
  return {
    type: CLEAR_SIGNUP_ERRORS_REQUEST,
  };
};
