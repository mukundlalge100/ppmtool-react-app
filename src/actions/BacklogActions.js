import {
  CREATE_PROJECT_TASK_REQUEST,
  GET_PROJECT_TASKS_REQUEST,
  CLEAR_CREATE_PROJECT_TASK_ERRORS_REQUEST,
  GET_PROJECT_TASK_REQUEST,
  UPDATE_PROJECT_TASK_REQUEST,
  DELETE_PROJECT_TASK_REQUEST,
} from "../constants/ActionTypes";

export const getProjectTasks = (projectIdentifier) => {
  return {
    type: GET_PROJECT_TASKS_REQUEST,
    projectIdentifier,
  };
};

export const getProjectTask = (projectIdentifier, projectSequence) => {
  return {
    type: GET_PROJECT_TASK_REQUEST,
    projectIdentifier,
    projectSequence,
  };
};
export const updateProjectTask = (
  projectTask,
  projectIdentifier,
  projectSequence,
  history
) => {
  return {
    type: UPDATE_PROJECT_TASK_REQUEST,
    projectTask,
    projectIdentifier,
    projectSequence,
    history,
  };
};

export const deleteProjectTask = (projectIdentifier, projectSequence) => {
  return {
    type: DELETE_PROJECT_TASK_REQUEST,
    projectIdentifier,
    projectSequence,
  };
};

export const createProjectTask = (projectTask, projectIdentifier, history) => {
  return {
    type: CREATE_PROJECT_TASK_REQUEST,
    projectIdentifier,
    projectTask,
    history,
  };
};
export const clearErrors = () => {
  return {
    type: CLEAR_CREATE_PROJECT_TASK_ERRORS_REQUEST,
  };
};
