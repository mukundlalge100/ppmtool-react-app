import {
  GET_PROJECTS_REQUEST,
  UPDATE_PROJECT_REQUEST,
  GET_PROJECT_REQUEST,
  CREATE_PROJECT_REQUEST,
  DELETE_PROJECT_REQUEST,
  CLEAR_CREATE_PROJECT_ERRORS_REQUEST,
} from "../constants/ActionTypes";

export const getProjects = (pageNumber) => {
  return {
    type: GET_PROJECTS_REQUEST,
    pageNumber,
  };
};
export const getProject = (projectIdentifier) => {
  return {
    type: GET_PROJECT_REQUEST,
    projectIdentifier,
  };
};
export const updateProject = (project, projectIdentifier, history) => {
  return {
    type: UPDATE_PROJECT_REQUEST,
    project,
    projectIdentifier,
    history,
  };
};
export const createProject = (project, history) => {
  return {
    type: CREATE_PROJECT_REQUEST,
    project,
    history,
  };
};
export const deleteProject = (projectIdentifier, currentPage) => {
  return {
    type: DELETE_PROJECT_REQUEST,
    projectIdentifier,
    currentPage,
  };
};
export const clearCreateProjectErrors = () => {
  return {
    type: CLEAR_CREATE_PROJECT_ERRORS_REQUEST,
  };
};
