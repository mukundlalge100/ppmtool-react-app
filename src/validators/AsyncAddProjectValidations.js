import axios from "../api/PPMTool";

const asyncAddProjectValidations = async value => {
  const errors = {};
  try {
    const requestBody = {
      projectIdentifier: value
    };
    const response = await axios.post("/api/project/checkID", requestBody);

    if (response.data.success) {
      errors.projectIdentifier =
        "This project ID already taken try another one!!";
      return errors;
    } else {
      return null;
    }
  } catch (error) {
    console.log(error);
  }
};
export default asyncAddProjectValidations;
