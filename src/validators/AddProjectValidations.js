import { isLength } from "validator";
import { isEmpty } from "../utils/Util";
import asyncAddProjectValidations from "./AsyncAddProjectValidations";

const addProjectValidations = ({
  projectName,
  projectIdentifier,
  description,
  startDate
}) => {
  const errors = {};
  if (isEmpty(projectName)) {
    errors.projectName = "Project Name is required!";
  } else if (!isLength(projectName, { min: 4, max: 60 })) {
    errors.projectName = "Project Name must be in between 4 and 60 characters!";
  }

  if (isEmpty(projectIdentifier)) {
    errors.projectIdentifier = "Project identifier is required!";
  } else if (!isLength(projectIdentifier, { min: 4, max: 5 })) {
    errors.projectIdentifier =
      "Project Identifier should be 4 or 5 characters!";
  }

  if (isEmpty(description)) {
    errors.description = "Description is required!";
  } else if (!isLength(description, { min: 10, max: 400 })) {
    errors.description =
      "Description must be in between 10 and 400 characters!";
  }
  if (isEmpty(startDate)) {
    errors.startDate = "Start Date is required!";
  }
  return Object.keys(errors).length > 0
    ? errors
    : asyncAddProjectValidations(projectIdentifier);
};
export default addProjectValidations;
