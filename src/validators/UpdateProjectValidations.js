import { isLength } from "validator";
import { isEmpty } from "../utils/Util";

const updateProjectValidations = ({ projectName, description, startDate }) => {
  const errors = {};
  if (isEmpty(projectName)) {
    errors.projectName = "Project Name is required!";
  } else if (!isLength(projectName, { min: 4, max: 60 })) {
    errors.projectName = "Project Name must be in between 4 and 60 characters!";
  }

  if (isEmpty(description)) {
    errors.description = "Description is required!";
  } else if (!isLength(description, { min: 10, max: 400 })) {
    errors.description =
      "Description must be in between 10 and 400 characters!";
  }

  if (isEmpty(startDate)) {
    errors.startDate = "Start Date is required!";
  }
};
export default updateProjectValidations;
