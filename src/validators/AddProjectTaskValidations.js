import { isLength } from "validator";
import { isEmpty } from "../utils/Util";

const addProjectTaskValidations = ({ summary, acceptenceCriteria }) => {
  const errors = {};

  if (isEmpty(summary)) {
    errors.summary = "Summary is required!";
  } else if (!isLength(summary, { min: 4, max: 60 })) {
    errors.summary = "Project Name must be in between 4 and 60 characters!";
  }

  if (isEmpty(acceptenceCriteria)) {
    errors.acceptenceCriteria = "Acceptence criteria is required!";
  }
  return errors;
};
export default addProjectTaskValidations;
