import validator from "validator";
import { isEmpty } from "../utils/Util";

const logInValidations = ({ email, password }) => {
  const errors = {};
  if (isEmpty(email)) {
    errors.email = "Email is required!";
  } else if (!validator.isEmail(email)) {
    errors.email = "Email is not valid!";
  }

  if (isEmpty(password)) {
    errors.password = "Password is required!";
  }
  return errors;
};
export default logInValidations;
