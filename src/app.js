import React, { Component, Suspense, lazy } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Loader from "./components/UI/Loader/Loader";
import classes from "./app.module.scss";
import utilClasses from "./utils/Util.module.scss";
import Navbar from "./containers/Layouts/Navbar/Navbar";
import Footer from "./components/Layouts/Footer/Footer";
import Routing from "./components/Routing/Routing";

import { checkLogInStateRequest } from "./actions/AuthActions";

class App extends Component {
  componentDidMount = () => {
    this.props.checkLogInStateRequest(this.props.history);
  };
  componentDidUpdate = (prevProps) => {
    if (prevProps.history !== this.props.history) {
      this.props.checkLogInStateRequest(this.props.history);
    }
  };
  render() {
    const { isLoadComponent, isLoading } = this.props;
    if (isLoading) {
      <div className={utilClasses.Loader__Centered}>
        <Loader />
      </div>;
    }
    if (isLoadComponent) {
      return (
        <main className={classes.App}>
          <Navbar />
          <Routing isAuthenticated={this.props.isAuthenticated} />
          <Footer />
        </main>
      );
    }
    return null;
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.authReducer.isLoading,
    isLoadComponent: state.authReducer.isLoadComponent,
    isAuthenticated: state.authReducer.isAuthenticated,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    checkLogInStateRequest: (history) =>
      dispatch(checkLogInStateRequest(history)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
