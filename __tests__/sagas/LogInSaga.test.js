import * as api from "../../src/api/AuthApi";
import { logIn } from "../../src/sagas/LogInSaga";
import { utilSaga } from "./Utility.test";
import { LOGIN_FAIL, LOGIN_SUCCESS } from "../../src/constants/ActionTypes";
import * as util from "../../src/utils/Util";

describe("LogIn saga test cases ->", () => {
  const initialAction = {
    userData: {},
    history: {
      push: jest.fn(),
    },
  };
  api.logInRequest = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("LogIn saga success call test", async () => {
    const response = {
      data: {
        token:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmIwZDgwZGEwYzUyMjAwMTcxMWM0M2YiLCJ1c2VyTmFtZSI6Im11a3VuZGxhbGdlMTAwIiwiYXZhdGFyIjoiLy93d3cuZ3JhdmF0YXIuY29tL2F2YXRhci8wYzc4MTA4NWJhYmI4ZGM1MjM2NGFjODZkYWQ0YThmNz9zaXplPTIwMCZyYXRpbmc9cGcmZGVmYXVsdD1tbSIsImlhdCI6MTYwOTU3ODM5MSwiZXhwIjoxNjA5NTg1NTkxfQ.iKEUWVXaEUFRCkfymZmB2gLjSCf6x495oRpZCfDqd1I",
      },
    };
    jest.mock("jwt-decode", () => () => ({}));
    util.setAuthToken = jest.fn();

    api.logInRequest.mockImplementation(() => Promise.resolve(response));
    const dispatched = await utilSaga(logIn, initialAction);
    expect(api.logInRequest).toBeCalledTimes(1);
    expect(api.logInRequest).toBeCalledWith({});
    expect(dispatched).toEqual([
      {
        type: LOGIN_SUCCESS,
        user: {
          _id: "5fb0d80da0c522001711c43f",
          avatar:
            "//www.gravatar.com/avatar/0c781085babb8dc52364ac86dad4a8f7?size=200&rating=pg&default=mm",
          exp: 1609585591,
          iat: 1609578391,
          userName: "mukundlalge100",
        },
      },
    ]);
  });

  it("LogIn saga reject call test", async () => {
    const error = new Error();
    error.response = {
      data: "Error message",
    };
    api.logInRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(logIn, initialAction);
    expect(api.logInRequest).toBeCalledTimes(1);
    expect(api.logInRequest).toBeCalledWith({});
    expect(dispatched).toEqual([
      {
        type: LOGIN_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
