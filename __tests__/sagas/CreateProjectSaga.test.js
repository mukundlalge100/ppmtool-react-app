import * as api from "../../src/api/ProjectApi";
import { createProject } from "../../src/sagas/CreateProjectSaga";
import { utilSaga } from "./Utility.test";
import {
  CREATE_PROJECT_FAIL,
  CREATE_PROJECT_SUCCESS,
} from "../../src/constants/ActionTypes";

describe("CreateProject saga test cases ->", () => {
  const initialAction = {
    project: {},
    history: {
      push: jest.fn(),
    },
  };
  api.createProjectRequest = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("CreateProject saga success call test", async () => {
    const response = {
      data: {
        projectIdentifier: "abcde",
        projectName: "Demo Project",
      },
    };
    api.createProjectRequest.mockImplementation(() =>
      Promise.resolve(response)
    );
    const dispatched = await utilSaga(createProject, initialAction);
    expect(api.createProjectRequest).toBeCalledTimes(1);
    expect(api.createProjectRequest).toBeCalledWith({});
    expect(dispatched).toEqual([
      {
        type: CREATE_PROJECT_SUCCESS,
        project: response.data,
      },
    ]);
  });

  it("CreateProject saga reject call test", async () => {
    const error = new Error();
    error.response = {
      data: "Dummy Data",
    };
    api.createProjectRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(createProject, initialAction);
    expect(api.createProjectRequest).toBeCalledTimes(1);
    expect(api.createProjectRequest).toBeCalledWith({});
    expect(dispatched).toEqual([
      {
        type: CREATE_PROJECT_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
