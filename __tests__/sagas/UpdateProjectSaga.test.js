import * as api from "../../src/api/ProjectApi";
import { updateProject } from "../../src/sagas/UpdateProjectSaga";
import { utilSaga } from "./Utility.test";
import {
  UPDATE_PROJECT_FAIL,
  UPDATE_PROJECT_SUCCESS,
} from "../../src/constants/ActionTypes";

describe("UpdateProject saga test cases ->", () => {
  const initialAction = {
    projectIdentifier: "abcde",
    project: {},
    history: {
      push: jest.fn(),
    },
  };
  api.updateProjectRequest = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("UpdateProject saga success call test", async () => {
    const response = {
      data: {
        project: {},
      },
    };
    api.updateProjectRequest.mockImplementation(() =>
      Promise.resolve(response)
    );
    const dispatched = await utilSaga(updateProject, initialAction);
    expect(api.updateProjectRequest).toBeCalledTimes(1);
    expect(api.updateProjectRequest).toBeCalledWith({}, "abcde");
    expect(dispatched).toEqual([
      {
        type: UPDATE_PROJECT_SUCCESS,
        project: response.data,
      },
    ]);
  });

  it("UpdateProject saga reject call test", async () => {
    const error = new Error();
    error.response = {
      data: "Error message",
    };
    api.updateProjectRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(updateProject, initialAction);
    expect(api.updateProjectRequest).toBeCalledTimes(1);
    expect(api.updateProjectRequest).toBeCalledWith({}, "abcde");
    expect(dispatched).toEqual([
      {
        type: UPDATE_PROJECT_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
