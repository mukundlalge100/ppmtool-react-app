import { runSaga } from "redux-saga";

export async function utilSaga(saga, initialAction) {
  const dispatched = [];

  await runSaga(
    {
      dispatch: (action) => dispatched.push(action),
    },
    saga,
    initialAction
  ).done;

  return dispatched;
}

describe("util Saga test cases ->", () => {
  it("Util Saga test", () => {});
});
