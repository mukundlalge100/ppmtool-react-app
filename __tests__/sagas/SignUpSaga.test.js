import * as api from "../../src/api/AuthApi";
import { signUp } from "../../src/sagas/SignUpSaga";
import { utilSaga } from "./Utility.test";
import { SIGNUP_FAIL, SIGNUP_SUCCESS } from "../../src/constants/ActionTypes";

describe("SignUp saga test cases ->", () => {
  const initialAction = {
    userData: {},
    history: {
      push: jest.fn(),
    },
  };
  api.signUpRequest = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("SignUp saga success call test", async () => {
    const response = {
      data: {
        message: "signup successfully!",
      },
    };
    api.signUpRequest.mockImplementation(() => Promise.resolve(response));
    const dispatched = await utilSaga(signUp, initialAction);
    expect(api.signUpRequest).toBeCalledTimes(1);
    expect(api.signUpRequest).toBeCalledWith({});
    expect(dispatched).toEqual([
      {
        type: SIGNUP_SUCCESS,
        data: response.data,
      },
    ]);
  });

  it("SignUp saga reject call test", async () => {
    const error = new Error();
    error.response = {
      data: "Error message",
    };
    api.signUpRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(signUp, initialAction);
    expect(api.signUpRequest).toBeCalledTimes(1);
    expect(api.signUpRequest).toBeCalledWith({});
    expect(dispatched).toEqual([
      {
        type: SIGNUP_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
