import * as api from "../../src/api/ProjectApi";
import { getProjects } from "../../src/sagas/GetProjectsSaga";
import { utilSaga } from "./Utility.test";
import {
  GET_PROJECTS_FAIL,
  GET_PROJECTS_SUCCESS,
} from "../../src/constants/ActionTypes";

describe("GetProjectsSaga test cases ->", () => {
  const initialAction = { pageNumber: 1 };
  api.getProjectsRequest = jest.fn();
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("Get all projects saga success call test", async () => {
    const response = {
      data: {
        content: [],
        totalPages: 3,
        size: 4,
        totalElements: 5,
      },
    };
    api.getProjectsRequest.mockImplementation(() => Promise.resolve(response));
    const dispatched = await utilSaga(getProjects, initialAction);
    expect(api.getProjectsRequest).toBeCalledTimes(1);
    expect(api.getProjectsRequest).toBeCalledWith(1);
    expect(dispatched).toEqual([
      {
        type: GET_PROJECTS_SUCCESS,
        projects: [],
        totalPages: 3,
        itemsCountPerPage: 4,
        totalItemsCount: 5,
      },
    ]);
  });

  it("Get all projects saga reject test", async () => {
    const error = new Error();
    error.response = {
      data: "Dummy Data",
    };
    api.getProjectsRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(getProjects, initialAction);
    expect(api.getProjectsRequest).toBeCalledTimes(1);
    expect(api.getProjectsRequest).toBeCalledWith(1);
    expect(dispatched).toEqual([
      {
        type: GET_PROJECTS_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
