import * as api from "../../src/api/BacklogApi";
import { getProjectTasks } from "../../src/sagas/GetProjectTasksSaga";
import { utilSaga } from "./Utility.test";
import {
  GET_PROJECT_TASKS_FAIL,
  GET_PROJECT_TASKS_SUCCESS,
} from "../../src/constants/ActionTypes";

describe("GetProjectTasks saga test cases ->", () => {
  const initialAction = {
    projectIdentifier: "abcde",
  };
  api.getProjectTasksRequest = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("GetProjectTasks saga success call test", async () => {
    const response = {
      data: {
        projectTasks: [],
      },
    };
    api.getProjectTasksRequest.mockImplementation(() =>
      Promise.resolve(response)
    );
    const dispatched = await utilSaga(getProjectTasks, initialAction);
    expect(api.getProjectTasksRequest).toBeCalledTimes(1);
    expect(api.getProjectTasksRequest).toBeCalledWith("abcde");
    expect(dispatched).toEqual([
      {
        type: GET_PROJECT_TASKS_SUCCESS,
        projectTasks: response.data,
      },
    ]);
  });

  it("GetProjectTasks saga reject call test", async () => {
    const error = new Error();
    error.response = {
      data: "Error message",
    };
    api.getProjectTasksRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(getProjectTasks, initialAction);
    expect(api.getProjectTasksRequest).toBeCalledTimes(1);
    expect(api.getProjectTasksRequest).toBeCalledWith("abcde");
    expect(dispatched).toEqual([
      {
        type: GET_PROJECT_TASKS_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
