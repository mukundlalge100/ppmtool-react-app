import * as api from "../../src/api/AuthApi";
import { logOut } from "../../src/sagas/LogOutSaga";
import { utilSaga } from "./Utility.test";
import { LOGOUT_FAIL, LOGOUT_SUCCESS } from "../../src/constants/ActionTypes";
import * as util from "../../src/utils/Util";

describe("LogOut saga test cases ->", () => {
  const initialAction = {
    history: {
      push: jest.fn(),
    },
  };
  api.logOutRequest = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("LogOut saga success call test", async () => {
    const response = {
      data: {
        success: true,
      },
    };
    api.logOutRequest.mockImplementation(() => Promise.resolve(response));
    const dispatched = await utilSaga(logOut, initialAction);
    expect(api.logOutRequest).toBeCalledTimes(1);
    expect(api.logOutRequest).toBeCalledWith();
    expect(dispatched).toEqual([
      {
        type: LOGOUT_SUCCESS,
      },
    ]);
  });

  it("LogOut saga reject call test", async () => {
    const error = new Error();
    error.response = {
      data: "Error message",
    };
    api.logOutRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(logOut, initialAction);
    expect(api.logOutRequest).toBeCalledTimes(1);
    expect(api.logOutRequest).toBeCalledWith();
    expect(dispatched).toEqual([
      {
        type: LOGOUT_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
