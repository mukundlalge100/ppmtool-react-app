import * as api from "../../src/api/ProjectApi";
import { deleteProject } from "../../src/sagas/DeleteProjectSaga";
import { utilSaga } from "./Utility.test";
import {
  DELETE_PROJECT_FAIL,
  DELETE_PROJECT_SUCCESS,
  GET_PROJECTS_REQUEST,
} from "../../src/constants/ActionTypes";

describe("DeleteProject saga test cases ->", () => {
  const initialAction = {
    projectIdentifier: "abcde",
    currentPage: 1,
    history: {
      push: jest.fn(),
    },
  };
  api.deleteProjectRequest = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("DeleteProject saga success call test", async () => {
    const response = {
      data: {
        success: true,
      },
    };
    api.deleteProjectRequest.mockImplementation(() =>
      Promise.resolve(response)
    );
    const dispatched = await utilSaga(deleteProject, initialAction);
    expect(api.deleteProjectRequest).toBeCalledTimes(1);
    expect(api.deleteProjectRequest).toBeCalledWith("abcde");
    expect(dispatched).toEqual([
      {
        type: DELETE_PROJECT_SUCCESS,
      },
      { type: GET_PROJECTS_REQUEST, pageNumber: 1 },
    ]);
  });

  it("CreateProject saga reject call test", async () => {
    const error = new Error();
    error.response = {
      data: "Error message",
    };
    api.deleteProjectRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(deleteProject, initialAction);
    expect(api.deleteProjectRequest).toBeCalledTimes(1);
    expect(api.deleteProjectRequest).toBeCalledWith("abcde");
    expect(dispatched).toEqual([
      {
        type: DELETE_PROJECT_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
