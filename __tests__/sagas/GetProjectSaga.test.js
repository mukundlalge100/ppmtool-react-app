import * as api from "../../src/api/ProjectApi";
import { getProject } from "../../src/sagas/GetProjectSaga";
import { utilSaga } from "./Utility.test";
import {
  GET_PROJECT_FAIL,
  GET_PROJECT_SUCCESS,
} from "../../src/constants/ActionTypes";

describe("GetProjectSaga test cases ->", () => {
  const initialAction = { projectIdentifier: "abcde" };
  api.getProjectRequest = jest.fn();
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it("Should get project for valid profileIdentifier", async () => {
    const response = {
      data: {
        projectIdentifier: "abcde",
        projectName: "Demo Project",
      },
    };
    api.getProjectRequest.mockImplementation(() => Promise.resolve(response));
    const dispatched = await utilSaga(getProject, initialAction);
    expect(api.getProjectRequest).toBeCalledTimes(1);
    expect(api.getProjectRequest).toBeCalledWith("abcde");
    expect(dispatched).toEqual([
      {
        type: GET_PROJECT_SUCCESS,
        project: response.data,
      },
    ]);
  });

  it("Should fail saga call for bad profileIdentifier", async () => {
    const error = new Error();
    error.response = {
      data: "Dummy Data",
    };
    api.getProjectRequest.mockImplementation(() => Promise.reject(error));
    const dispatched = await utilSaga(getProject, initialAction);
    expect(api.getProjectRequest).toBeCalledTimes(1);
    expect(api.getProjectRequest).toBeCalledWith("abcde");
    expect(dispatched).toEqual([
      {
        type: GET_PROJECT_FAIL,
        errors: error.response.data,
      },
    ]);
  });
});
