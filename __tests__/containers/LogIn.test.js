import { LogIn } from "../../src/containers/Auth/LogIn/LogIn";
import { setup } from "../../src/utils/utilsTest";

describe("LogIn container test ", () => {
  const basicProps = {
    isAuthenticated: false,
    isLoading: false,
    errors: {},
    onClearLogInErrors: jest.fn(),
    logInRequest: jest.fn(),
    history: {},
  };
  const initialStore = {
    authReducer: {
      data: {},
      user: {},
      isLoading: false,
      errors: {},
      isAuthenticated: false,
    },
  };
  let globalObj = { store: null, wrapper: null };

  afterEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  beforeEach(() => {
    let { store, wrapper } = setup(basicProps, LogIn, initialStore);
    globalObj = {
      store,
      wrapper,
    };
  });

  test("Render LogIn component without errors", () => {});

  test("On submit call logInRequest action ", () => {
    const { wrapper } = globalObj;

    const email = wrapper.find('input[name="email"]');
    email.instance().value = "mukund@gmail.com";
    email.simulate("change", email);
    const password = wrapper.find('input[name="password"]');
    password.instance().value = "12345678";
    password.simulate("change", password);
    wrapper.find("form").simulate("submit");

    expect(basicProps.onClearLogInErrors).toHaveBeenCalledTimes(1);
    expect(basicProps.logInRequest).toHaveBeenCalledTimes(1);
  });
});
