import * as actions from "../../src/actions/AuthActions";
import {
  LOGIN_REQUEST,
  SIGNUP_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT_REQUEST,
  CHECK_LOGIN_STATE_REQUEST,
  CLEAR_LOGIN_ERRORS_REQUEST,
  CLEAR_SIGNUP_ERRORS_REQUEST,
} from "../../src/constants/ActionTypes";

describe("Auth actions tests started", () => {
  it("Create action for logInRequest", () => {
    const expectedAction = {
      type: LOGIN_REQUEST,
      userData: null,
      history: null,
    };
    expect(actions.logInRequest(null, null)).toEqual(expectedAction);
  });

  it("Create action for signUpRequest", () => {
    const expectedAction = {
      type: SIGNUP_REQUEST,
      userData: null,
      history: null,
    };
    expect(actions.signUpRequest(null, null)).toEqual(expectedAction);
  });

  it("Create action for logInSuccess", () => {
    const expectedAction = {
      type: LOGIN_SUCCESS,
      user: null,
    };
    expect(actions.logInSuccess(null)).toEqual(expectedAction);
  });

  it("Create action for logOutRequest", () => {
    const expectedAction = {
      type: LOGOUT_REQUEST,
      history: null,
    };
    expect(actions.logOutRequest(null)).toEqual(expectedAction);
  });

  it("Create action for checkLogInStateRequest", () => {
    const expectedAction = {
      type: CHECK_LOGIN_STATE_REQUEST,
      history: null,
    };
    expect(actions.checkLogInStateRequest(null)).toEqual(expectedAction);
  });

  it("Create action for clearLogInErrors", () => {
    const expectedAction = {
      type: CLEAR_LOGIN_ERRORS_REQUEST,
    };
    expect(actions.clearLogInErrors()).toEqual(expectedAction);
  });

  it("Create action for clearSignUpErrors", () => {
    const expectedAction = {
      type: CLEAR_SIGNUP_ERRORS_REQUEST,
    };
    expect(actions.clearSignUpErrors()).toEqual(expectedAction);
  });
});
