import {
  getProject,
  getProjects,
  deleteProject,
  createProject,
  updateProject,
  clearCreateProjectErrors,
} from "../../src/actions/ProjectActions";
import {
  GET_PROJECTS_REQUEST,
  UPDATE_PROJECT_REQUEST,
  GET_PROJECT_REQUEST,
  CREATE_PROJECT_REQUEST,
  DELETE_PROJECT_REQUEST,
  CLEAR_CREATE_PROJECT_ERRORS_REQUEST,
} from "../../src/constants/ActionTypes";

describe("Project actions tests started", () => {
  it("Create action for getProjects", () => {
    const expectedAction = {
      type: GET_PROJECTS_REQUEST,
      pageNumber: null,
    };
    expect(getProjects(null)).toEqual(expectedAction);
  });

  it("Create action for getProject", () => {
    const expectedAction = {
      type: GET_PROJECT_REQUEST,
      projectIdentifier: null,
    };
    expect(getProject(null)).toEqual(expectedAction);
  });

  it("Create action for updateProject", () => {
    const expectedAction = {
      type: UPDATE_PROJECT_REQUEST,
      project: null,
      projectIdentifier: null,
      history: null,
    };
    expect(updateProject(null, null, null)).toEqual(expectedAction);
  });

  it("Create action for createProject", () => {
    const expectedAction = {
      type: CREATE_PROJECT_REQUEST,
      project: null,
      history: null,
    };
    expect(createProject(null, null)).toEqual(expectedAction);
  });

  it("Create action for deleteProject", () => {
    const expectedAction = {
      type: DELETE_PROJECT_REQUEST,
      projectIdentifier: null,
      currentPage: null,
    };
    expect(deleteProject(null, null)).toEqual(expectedAction);
  });

  it("Create action for clearCreateProjectErrors", () => {
    const expectedAction = {
      type: CLEAR_CREATE_PROJECT_ERRORS_REQUEST,
    };
    expect(clearCreateProjectErrors()).toEqual(expectedAction);
  });
});
