import * as actions from "../../src/actions/BacklogActions";
import { GET_PROJECT_TASKS_REQUEST } from "../../src/constants/ActionTypes";

describe("Backlog actions tests started", () => {
  it("Create action for getProjectTasks ", () => {
    const expectedAction = {
      type: GET_PROJECT_TASKS_REQUEST,
      projectIdentifier: null,
    };
    expect(actions.getProjectTasks(null)).toEqual(expectedAction);
  });
});
