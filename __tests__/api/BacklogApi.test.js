import axios from "../../src/api/PPMTool";
import { getProjectTasksRequest } from "../../src/api/BacklogApi";

describe("Api calls from axios for BacklogApi", () => {
  const responseData = {
    message: "Request done successfully!",
    success: true,
  };
  const projectIdentifier = "abcd";

  afterEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  test("getProjectTasksRequest() api call resolves", async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(getProjectTasksRequest(projectIdentifier)).resolves.toEqual(
      responseData
    );
    expect(axios.get.mock.calls[0]).toEqual([
      `/api/backlog/${projectIdentifier}`,
    ]);
    expect(axios.get).toHaveBeenCalledWith(`/api/backlog/${projectIdentifier}`);
  });

  test("getProjectTasksRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(getProjectTasksRequest(projectIdentifier)).rejects.toThrow(
      errorMessage
    );
  });
});
