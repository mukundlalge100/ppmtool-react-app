import axios from "../../src/api/PPMTool";
import {
  getProjectRequest,
  getProjectsRequest,
  createProjectRequest,
  updateProjectRequest,
  deleteProjectRequest,
} from "../../src/api/ProjectApi";

describe("Api calls from axios for ProjectApi", () => {
  const responseData = {
    message: "Request done successfully!",
    success: true,
  };
  const projectIdentifier = "abcd";
  const pageNumber = 1;

  const project = {
    projectName: "some demo project",
    projectIdentifier: "abcd",
    description: "some description",
    startDate: "10-12-2020",
    endDate: "10-05-2021",
  };

  afterEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  test("getProjectRequest() api call resolves", async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(getProjectRequest(projectIdentifier)).resolves.toEqual(
      responseData
    );
    expect(axios.get.mock.calls[0]).toEqual([
      `/api/project/${projectIdentifier}`,
    ]);
    expect(axios.get).toHaveBeenCalledWith(`/api/project/${projectIdentifier}`);
  });

  test("getProjectRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(getProjectRequest(projectIdentifier)).rejects.toThrow(
      errorMessage
    );
  });

  test("getProjectsRequest() api call resolves", async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(getProjectsRequest(pageNumber)).resolves.toEqual(responseData);
    expect(axios.get.mock.calls[0]).toEqual([`/api/project/all/${pageNumber}`]);
    expect(axios.get).toHaveBeenCalledWith(`/api/project/all/${pageNumber}`);
  });

  test("getProjectsRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(getProjectsRequest(pageNumber)).rejects.toThrow(errorMessage);
  });

  test("updateProjectRequest() api call resolves", async () => {
    axios.put.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(
      updateProjectRequest(project, projectIdentifier)
    ).resolves.toEqual(responseData);
    expect(axios.put.mock.calls[0]).toEqual([
      `/api/project/${projectIdentifier}`,
      project,
    ]);
    expect(axios.put).toHaveBeenCalledWith(
      `/api/project/${projectIdentifier}`,
      project
    );
  });

  test("updateProjectRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.put.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(
      updateProjectRequest(project, projectIdentifier)
    ).rejects.toThrow(errorMessage);
  });

  test("deleteProjectRequest() api call resolves", async () => {
    axios.delete.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(deleteProjectRequest(projectIdentifier)).resolves.toEqual(
      responseData
    );
    expect(axios.delete.mock.calls[0]).toEqual([
      `/api/project/${projectIdentifier}`,
    ]);
    expect(axios.delete).toHaveBeenCalledWith(
      `/api/project/${projectIdentifier}`
    );
  });

  test("deleteProjectRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.delete.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(deleteProjectRequest(projectIdentifier)).rejects.toThrow(
      errorMessage
    );
  });

  test("createProjectRequest() api call resolves", async () => {
    axios.post.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(createProjectRequest(project)).resolves.toEqual(responseData);
    expect(axios.post.mock.calls[0]).toEqual([`/api/project`, project]);
    expect(axios.post).toHaveBeenCalledWith(`/api/project`, project);
  });

  test("createProjectRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.post.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(createProjectRequest(project)).rejects.toThrow(errorMessage);
  });
});
