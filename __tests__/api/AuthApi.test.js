import axios from "../../src/api/PPMTool";
import {
  logInRequest,
  signUpRequest,
  logOutRequest,
} from "../../src/api/AuthApi";

describe("Api calls from axios for AuthApi", () => {
  const responseData = {
    message: "Request done successfully!",
    success: true,
  };

  afterEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  test("logInRequest() api call resolve", async () => {
    const userData = {
      email: "mukund@gmail.com",
      password: "Lms@1234",
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(logInRequest(userData)).resolves.toEqual(responseData);
    expect(axios.post.mock.calls[0]).toEqual([`/api/user/login`, userData]);
    expect(axios.post).toHaveBeenCalledWith(`/api/user/login`, userData);
  });

  test("logInRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.post.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(logInRequest("demoData")).rejects.toThrow(errorMessage);
  });

  test("signUpRequest() api call resolves", async () => {
    const userData = {
      userName: "mukundlalge",
      email: "mukund@gmail.com",
      password: "Lms@1234",
      confirmPassword: "Lms@1234",
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(signUpRequest(userData)).resolves.toEqual(responseData);
    expect(axios.post).toHaveBeenCalledWith(`/api/user/signup`, userData);
  });

  test("signUpRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.post.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(signUpRequest("demoData")).rejects.toThrow(errorMessage);
  });

  test("logOutRequest() api call resolves", async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(responseData));
    await expect(logOutRequest()).resolves.toEqual(responseData);
    expect(axios.get.mock.calls[0]).toEqual([`/api/user/logout`]);
    expect(axios.get).toHaveBeenCalledWith(`/api/user/logout`);
  });

  test("logOutRequest() api call rejects", async () => {
    const errorMessage = "Network Error";
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );
    await expect(logOutRequest()).rejects.toThrow(errorMessage);
  });
});
