import authReducer from "../../src/reducers/AuthReducer";
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAIL,
  CLEAR_LOGIN_ERRORS,
} from "../../src/constants/ActionTypes";

describe("AuthReducer test->", () => {
  const initialState = {
    data: {},
    user: {},
    isLoading: false,
    errors: {},
    isAuthenticated: false,
  };
  const errors = {
    message: "Something went wrong",
    success: false,
  };

  const data = {
    email: "mukundlalge@gmail.com",
    userName: "mukund",
  };

  test("should handle LOGIN_REQUEST ->", () => {
    const authAction = {
      type: LOGIN_REQUEST,
    };
    const updatedState = authReducer(initialState, authAction);
    expect(updatedState.isLoading).toEqual(true);
  });

  test("should handle LOGIN_SUCCESS ->", () => {
    const authAction = {
      type: LOGIN_SUCCESS,
      user: data,
    };
    const updatedState = authReducer(initialState, authAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.isAuthenticated).toEqual(true);
    expect(updatedState.user).toEqual(data);
  });

  test("should handle LOGIN_FAIL ->", () => {
    const authAction = {
      type: LOGIN_FAIL,
      errors,
    };
    const updatedState = authReducer(initialState, authAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });

  test("should handle CLEAR_LOGIN_ERRORS ->", () => {
    const authAction = {
      type: CLEAR_LOGIN_ERRORS,
    };
    const updatedState = authReducer(initialState, authAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual({});
  });

  test("should handle LOGOUT_REQUEST ->", () => {
    const authAction = {
      type: LOGOUT_REQUEST,
    };
    const updatedState = authReducer(initialState, authAction);
    expect(updatedState.isLoading).toEqual(true);
  });

  test("should handle LOGOUT_SUCCESS ->", () => {
    const authAction = {
      type: LOGOUT_SUCCESS,
      data,
    };
    const updatedState = authReducer(initialState, authAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.user).toEqual({});
    expect(updatedState.data).toEqual({});
    expect(updatedState.isAuthenticated).toEqual(false);
  });

  test("should handle LOGOUT_FAIL ->", () => {
    const authAction = {
      type: LOGOUT_FAIL,
      errors,
    };
    const updatedState = authReducer(initialState, authAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });
});
