import projectReducer from "../../src/reducers/ProjectReducer";
import {
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAIL,
  CLEAR_CREATE_PROJECT_ERRORS,
  UPDATE_PROJECT_REQUEST,
  UPDATE_PROJECT_SUCCESS,
  UPDATE_PROJECT_FAIL,
  GET_PROJECT_REQUEST,
  GET_PROJECT_SUCCESS,
  GET_PROJECT_FAIL,
  GET_PROJECTS_REQUEST,
  GET_PROJECTS_SUCCESS,
  GET_PROJECTS_FAIL,
  DELETE_PROJECT_REQUEST,
  DELETE_PROJECT_SUCCESS,
  DELETE_PROJECT_FAIL,
} from "../../src/constants/ActionTypes";

describe("projectReducer test ->", () => {
  const initialState = {
    projects: [],
    totalPages: null,
    itemsCountPerPage: null,
    totalItemsCount: null,
    project: {},
    isLoading: false,
    errors: {},
  };
  const errors = {
    message: "Something went wrong",
    success: false,
  };

  const data = {
    projectName: "Some demo project",
    projectIdentifier: "abcdw",
  };
  const arrayOfData = [
    {
      demoKey1: "some Data",
      demoKey2: "some data",
    },
    {
      demoKey1: "some Data",
      demoKey2: "some data",
    },
  ];

  test("should handle CREATE_PROJECT_REQUEST ->", () => {
    const projectAction = {
      type: CREATE_PROJECT_REQUEST,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(true);
  });

  test("should handle CREATE_PROJECT_SUCCESS ->", () => {
    const projectAction = {
      type: CREATE_PROJECT_SUCCESS,
      project: data,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.project).toEqual(data);
  });

  test("should handle CREATE_PROJECT_FAIL ->", () => {
    const projectAction = {
      type: CREATE_PROJECT_FAIL,
      errors,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });

  test("should handle CLEAR_CREATE_PROJECT_ERRORS ->", () => {
    const projectAction = {
      type: CLEAR_CREATE_PROJECT_ERRORS,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.errors).toEqual({});
  });

  test("should handle UPDATE_PROJECT_REQUEST ->", () => {
    const projectAction = {
      type: UPDATE_PROJECT_REQUEST,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(true);
  });
  test("should handle UPDATE_PROJECT_SUCCESS ->", () => {
    const projectAction = {
      type: UPDATE_PROJECT_SUCCESS,
      project: data,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.project).toEqual(data);
  });

  test("should handle UPDATE_PROJECT_FAIL ->", () => {
    const projectAction = {
      type: UPDATE_PROJECT_FAIL,
      errors,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });

  test("should handle DELETE_PROJECT_REQUEST ->", () => {
    const projectAction = {
      type: DELETE_PROJECT_REQUEST,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(true);
  });
  test("should handle DELETE_PROJECT_SUCCESS ->", () => {
    const projectAction = {
      type: DELETE_PROJECT_SUCCESS,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
  });
  test("should handle DELETE_PROJECT_FAIL ->", () => {
    const projectAction = {
      type: DELETE_PROJECT_FAIL,
      errors,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });

  test("should handle GET_PROJECT_REQUEST ->", () => {
    const projectAction = {
      type: GET_PROJECT_REQUEST,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(true);
  });

  test("should handle GET_PROJECT_SUCCESS ->", () => {
    const projectAction = {
      type: GET_PROJECT_SUCCESS,
      project: data,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.project).toEqual(data);
  });

  test("should handle GET_PROJECT_FAIL ->", () => {
    const projectAction = {
      type: GET_PROJECT_FAIL,
      errors,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });

  test("should handle GET_PROJECTS_REQUEST ->", () => {
    const projectAction = {
      type: GET_PROJECTS_REQUEST,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(true);
  });

  test("should handle GET_PROJECTS_SUCCESS ->", () => {
    const projectAction = {
      type: GET_PROJECTS_SUCCESS,
      projects: arrayOfData,
      totalPages: 1,
      itemsCountPerPage: 2,
      totalItemsCount: 2,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.projects).toEqual(arrayOfData);
    expect(updatedState.totalPages).toEqual(1);
    expect(updatedState.itemsCountPerPage).toEqual(2);
    expect(updatedState.totalItemsCount).toEqual(2);
  });

  test("should handle GET_PROJECTS_FAIL ->", () => {
    const projectAction = {
      type: GET_PROJECTS_FAIL,
      errors,
    };
    const updatedState = projectReducer(initialState, projectAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });
});
