import projectTaskReducer from "../../src/reducers/ProjectTaskReducer";
import {
  GET_PROJECT_TASKS_REQUEST,
  GET_PROJECT_TASKS_SUCCESS,
  GET_PROJECT_TASKS_FAIL,
} from "../../src/constants/ActionTypes";

describe("projectReducer test ->", () => {
  const initialState = {
    projectTasks: [],
    projectTask: {},
    isLoading: false,
    errors: {},
  };
  const data = {
    projectName: "Some demo project",
    projectIdentifier: "abcdw",
  };
  const arrayOfData = [
    {
      demoKey1: "some Data",
      demoKey2: "some data",
    },
    {
      demoKey1: "some Data",
      demoKey2: "some data",
    },
  ];
  const errors = {
    message: "Something went wrong",
    success: false,
  };
  test("should handle GET_PROJECT_TASKS_REQUEST ->", () => {
    const projectTaskAction = {
      type: GET_PROJECT_TASKS_REQUEST,
    };
    const updatedState = projectTaskReducer(initialState, projectTaskAction);
    expect(updatedState.isLoading).toEqual(true);
  });

  test("should handle GET_PROJECT_TASKS_SUCCESS ->", () => {
    const projectTaskAction = {
      type: GET_PROJECT_TASKS_SUCCESS,
      projectTasks: arrayOfData,
    };
    const updatedState = projectTaskReducer(initialState, projectTaskAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.projectTasks).toEqual(arrayOfData);
  });

  test("should handle GET_PROJECT_TASKS_FAIL ->", () => {
    const projectTaskAction = {
      type: GET_PROJECT_TASKS_FAIL,
      errors,
    };
    const updatedState = projectTaskReducer(initialState, projectTaskAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });
});
