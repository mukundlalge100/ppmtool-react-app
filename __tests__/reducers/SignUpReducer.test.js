import signUpReducer from "../../src/reducers/SignUpReducer";
import {
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  CLEAR_SIGNUP_ERRORS,
} from "../../src/constants/ActionTypes";

describe("AuthReducer test->", () => {
  const initialState = {
    data: {},
    isLoading: false,
    errors: {},
  };
  const errors = {
    message: "Something went wrong",
    success: false,
  };

  const data = {
    email: "mukundlalge@gmail.com",
    userName: "mukund",
  };

  test("should handle SIGNUP_REQUEST ->", () => {
    const signUpAction = {
      type: SIGNUP_REQUEST,
    };
    const updatedState = signUpReducer(initialState, signUpAction);
    expect(updatedState.isLoading).toEqual(true);
  });

  test("should handle SIGNUP_SUCCESS ->", () => {
    const signUpAction = {
      type: SIGNUP_SUCCESS,
      data,
    };
    const updatedState = signUpReducer(initialState, signUpAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.data).toEqual(data);
  });

  test("should handle SIGNUP_FAIL ->", () => {
    const signUpAction = {
      type: SIGNUP_FAIL,
      errors,
    };
    const updatedState = signUpReducer(initialState, signUpAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual(errors);
  });

  test("should handle CLEAR_SIGNUP_ERRORS ->", () => {
    const signUpAction = {
      type: CLEAR_SIGNUP_ERRORS,
    };
    const updatedState = signUpReducer(initialState, signUpAction);
    expect(updatedState.isLoading).toEqual(false);
    expect(updatedState.errors).toEqual({});
  });
});
