# ppmtool-react-app

React app for Personal Project Management tool using redux-saga

# To start application locally ...

1. Clone repository from git to local
2. Run npm install to install all dependency
3. Run npm start to start application
